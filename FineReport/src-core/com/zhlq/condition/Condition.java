package com.zhlq.condition;

public class Condition {

	private String key;
	private String compare;
	private Object value;

	public Condition() {
		super();
	}

	public Condition(String key, Object value) {
		super();
		this.key = key;
		this.value = value;
	}

	public Condition(String key, String compare, Object value) {
		super();
		this.key = key;
		this.compare = compare;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCompare() {
		return compare;
	}

	public void setCompare(String compare) {
		this.compare = compare;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}