package com.zhlq.exception.bean;

/**
 * Exception entity. @author MyEclipse Persistence Tools
 */

public class Exception implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String msg;
	private String time;
	private String path;

	// Constructors

	/** default constructor */
	public Exception() {
	}

	/** full constructor */
	public Exception(String name, String msg, String time, String path) {
		this.name = name;
		this.msg = msg;
		this.time = time;
		this.path = path;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}