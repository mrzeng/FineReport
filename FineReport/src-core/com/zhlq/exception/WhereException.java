package com.zhlq.exception;

public class WhereException extends BaseException {

	/**
	 * @Fields serialVersionUID : 序列化ID
	 */
	private static final long serialVersionUID = 1L;

	public WhereException() {
		super();
	}

	public WhereException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public WhereException(String message, Throwable cause) {
		super(message, cause);
	}

	public WhereException(String message) {
		super(message);
	}

	public WhereException(Throwable cause) {
		super(cause);
	}
	
}
