package com.zhlq.exception;

/**
 * @ClassName BaseException
 * @Description 基础异常
 * @author ZHLQ
 * @date 2014年9月11日 下午9:54:19
 */
public class BaseException extends RuntimeException {

	/**
	 * @Fields serialVersionUID : 序列化编号
	 */
	private static final long serialVersionUID = 1L;
	
	public BaseException() {
		super();
	}

	public BaseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public BaseException(String message) {
		super(message);
	}

	public BaseException(Throwable cause) {
		super(cause);
	}

	public void doing(){
		// 保存到数据库或者是打印到控制台等
		this.printStackTrace();
	}

}
