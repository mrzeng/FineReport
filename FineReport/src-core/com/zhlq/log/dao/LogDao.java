package com.zhlq.log.dao;

import com.zhlq.core.dao.ProjectDao;

public interface LogDao extends ProjectDao {

	public void info(String msg);

}
