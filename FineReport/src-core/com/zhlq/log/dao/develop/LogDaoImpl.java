package com.zhlq.log.dao.develop;

import java.util.logging.Logger;

import com.zhlq.core.dao.hbntmpl.ProjectDaoImpl;
import com.zhlq.log.dao.LogDao;

public class LogDaoImpl extends ProjectDaoImpl implements LogDao {

	private Logger logger = Logger.getLogger(LogDaoImpl.class.getName());

	@Override
	public void info(String msg) {
		logger.info(msg);
	}
	
}
