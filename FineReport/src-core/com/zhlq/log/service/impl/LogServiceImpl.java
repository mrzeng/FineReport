package com.zhlq.log.service.impl;

import javax.servlet.http.HttpServletRequest;

import com.zhlq.core.service.impl.ProjectServiceImpl;
import com.zhlq.log.bean.Visit;
import com.zhlq.log.dao.LogDao;
import com.zhlq.log.service.LogService;

public class LogServiceImpl extends ProjectServiceImpl implements LogService {

	private LogDao logDao;

	public LogServiceImpl() {
		super();
	}

	public LogServiceImpl(String name) {
		super();
	}

	public void info(String msg) {
		logDao.info(msg);
	}

	@Override
	public Visit getVisit(HttpServletRequest request) {
		// 记录用户的访问操作
		Visit visit = new Visit();
		visit.setUrl(request.getRequestURL().toString());// getRequestURL方法返回客户端发出请求时的完整URL。
		visit.setUri(request.getRequestURI());// getRequestURI方法返回请求行中的资源名部分。
		visit.setQueryString(request.getQueryString());// getQueryString 方法返回请求行中的参数部分。
		visit.setRemoteAddr(request.getRemoteAddr());// getRemoteAddr方法返回发出请求的客户机的IP地址
		visit.setRemoteHost(request.getRemoteHost());// getRemoteHost方法返回发出请求的客户机的完整主机名
		visit.setRemotePort(String.valueOf(request.getRemotePort()));// getRemotePort方法返回客户机所使用的网络端口号
		visit.setLocalAddr(request.getLocalAddr());// getLocalAddr方法返回WEB服务器的IP地址。
		visit.setLocalName(request.getLocalName());// getLocalName方法返回WEB服务器的主机名
		visit.setMethod(request.getMethod());// getMethod得到客户机请求方式
		return visit;
	}

	public LogDao getLogDao() {
		return logDao;
	}

	public void setLogDao(LogDao logDao) {
		this.logDao = logDao;
	}

}
