package com.zhlq.validator.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zhlq.util.StringUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;

/**
 * @ClassName ValidatorServiceImpl
 * @Description 默认验证
 * @author ZHLQ
 * @date 2015年4月15日 下午10:47:58
 */
public class ValidatorServiceImpl implements ValidatorService {

	protected Map<String, String> map = new HashMap<String, String>();

	{
		map.put(ADD, ADD);
		map.put(DELETE, DELETE);
		map.put(LOOK, LOOK);
		map.put(TOEIDT, TOEIDT);
		map.put(EDIT, EDIT);
		map.put(OPEN, OPEN);
		map.put(CLOSE, CLOSE);
	}

	@Override
	public String get(String key) {
		return map.get(key);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
		} else if (this.get(DELETE).equals(type)) {
			// 删除
		} else if (this.get(LOOK).equals(type)) {
			// 查看
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
		} else if (this.get(OPEN).equals(type)) {
			// 启用
		} else if (this.get(CLOSE).equals(type)) {
			// 禁用
		} else {
			// 其他
		}
		return new ArrayList<Validator>();
	}

	@Override
	public boolean isPattern(String value, String pattern) {
		return value.matches(pattern);
	}

	@Override
	public boolean isOneOrTwoDigit(String value) {
		return value.matches(ONE_OR_TWO_DIGIT);
	}

	/**
	 * 对象
	 */
	protected List<Validator> validateObject(Object object, List<Validator> validator) {
		if (null == object) {
			validator.add(new Validator("", "对象", "不能为空"));
		}
		return validator;
	}

	/**
	 * 主键
	 */
	protected List<Validator> validateId(Integer id, List<Validator> validator) {
		if (null == id) {
			validator.add(new Validator("id", "主键", "不能为空"));
		} else if (id < 1) {
			validator.add(new Validator("id", "主键", "必须大于0"));
		}
		return validator;
	}

	/**
	 * 名称
	 */
	protected List<Validator> validateName(String name, List<Validator> validator) {
		if (StringUtil.isEmptyNull(name)) {
			validator.add(new Validator("name", "名称", "不能为空"));
		} else if (!name.matches("^[a-zA-Z][a-zA-Z0-9]{5,19}$")) {
			validator.add(new Validator("name", "名称", "必须为字母开头，其余为字母或者数字，长度6到20位"));
		}
		return validator;
	}


	/**
	 * 中文名称
	 */
	protected List<Validator> validateCname(String cname, List<Validator> validator) {
		if(StringUtil.isEmptyNull(cname)){
			validator.add(new Validator("cname", "中文名称", "不能为空"));
		} else if(!cname.matches("^[\u4E00-\u9FA5][\u4E00-\u9FA5]{0,31}$")){
			validator.add(new Validator("cname", "中文名称", "必须为中文字符，长度1到32位"));			
		}
		return validator;
	}


	/**
	 * 英文名称
	 */
	protected List<Validator> validateEname(String ename, List<Validator> validator) {
		if(StringUtil.isEmptyNull(ename)){
			validator.add(new Validator("ename", "英文名称", "不能为空"));
		} else if(!ename.matches("^[a-zA-Z][a-zA-Z0-9\\.]{0,127}$")){
			validator.add(new Validator("ename", "英文名称", "必须为字母开头，其余为字母或者数字或者.，长度1到128位"));			
		}
		return validator;
	}

	/**
	 * 密码
	 */
	protected List<Validator> validatePassword(String password, List<Validator> validator) {
		if (StringUtil.isEmptyNull(password) || "d41d8cd98f00b204e9800998ecf8427e".equals(password)) {
			validator.add(new Validator("password", "密码", "不能为空"));
		} else if (!password.matches("^[a-z0-9]{32}$")) {
			validator.add(new Validator("password", "密码", "必须为32位MD5加密密码"));
		}
		return validator;
	}

	/**
	 * 状态
	 */
	protected List<Validator> validateState(String state, List<Validator> validator) {
		if (StringUtil.isEmptyNull(state)) {
			validator.add(new Validator("state", "状态", "不能为空"));
		} else if (!state.matches("^[1|0]$")) {
			validator.add(new Validator("state", "状态", "必须为【启用/禁用】之中一个"));
		}
		return validator;
	}

	/**
	 * 类型
	 */
	protected List<Validator> validateType(String type, List<Validator> validator) {
		if(StringUtil.isEmptyNull(type)){
			validator.add(new Validator("type", "类型", "不能为空"));
		} else if(!type.matches("^[0-9]{1,2}$")){
			validator.add(new Validator("type", "类型", "必须为数字字符，长度为1到2位"));			
		}
		return validator;
	}

}
