package com.zhlq.core.dao.smctmpl;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.zhlq.condition.Where;
import com.zhlq.core.dao.ProjectDao;
import com.zhlq.exception.DataException;
import com.zhlq.exception.ReflectException;
import com.zhlq.util.PropUtil;

/**
 * @ClassName ProjectDaoImpl
 * @Description 项目DaoImpl
 * @author ZHLQ
 * @date 2014年9月6日 下午12:15:35
 */
public class ProjectDaoImpl extends BaseDaoImpl implements ProjectDao {

	@Override
	public Object retrieveOne(Object object) {
		List<Object> list = super.retrieve(object);
		if(null!=list && 1>=list.size()){
			return list.get(0);
		} else {
			return null;			
		}
	}

	@Override
	public Object retrieveOne(Where where) {
		List<Object> list = super.retrieve(where);
		if(null!=list && 1>=list.size()){
			return list.get(0);
		} else {
			return null;			
		}
	}

	@Override
	public Object retrieveOneException(Object object) {
		List<Object> list = super.retrieve(object);
		if(null!=list && 1>=list.size()){
			return list.get(0);
		} else {
			throw new DataException();
		}
	}

	@Override
	public Object retrieveOneException(Where where) {
		List<Object> list = super.retrieve(where);
		if(null!=list && 1>=list.size()){
			return list.get(0);
		} else {
			throw new DataException();
		}
	}

	@Override
	public Object retrieveOneForce(Object object) {
		List<Object> list = super.retrieve(object);
		if(null!=list && 1==list.size()){
			return list.get(0);
		} else {
			return null;			
		}
	}

	@Override
	public Object retrieveOneForce(Where where) {
		List<Object> list = super.retrieve(where);
		if(null!=list && 1==list.size()){
			return list.get(0);
		} else {
			return null;			
		}
	}

	@Override
	public Object retrieveOneForceException(Object object) {
		List<Object> list = super.retrieve(object);
		if(null!=list && 1==list.size()){
			return list.get(0);
		} else {
			throw new DataException();
		}
	}

	@Override
	public Object retrieveOneForceException(Where where) {
		List<Object> list = super.retrieve(where);
		if(null!=list && 1==list.size()){
			return list.get(0);
		} else {
			throw new DataException();
		}
	}

	@Override
	public void updateNotNullStr(Object object) {
		Where where = new Where(object.getClass());
		Object value = null;
		try {
			Method method = object.getClass().getDeclaredMethod("getId");
			value = method.invoke(object);
		} catch (SecurityException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new ReflectException("方法调用", e);
		}
		where.addEqual("id", value);
		Object dbObject = this.retrieveOne(where);
		try {
			PropUtil.copy(dbObject, object);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | IntrospectionException e) {
			(new ReflectException("复制属性", e)).doing();
		}
		super.update(dbObject);
	}

}
