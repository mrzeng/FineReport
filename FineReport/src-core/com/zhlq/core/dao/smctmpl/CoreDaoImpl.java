package com.zhlq.core.dao.smctmpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.ibatis.SqlMapClientTemplate;

import com.zhlq.condition.Sql;
import com.zhlq.condition.Where;
import com.zhlq.log.dao.LogDao;
import com.zhlq.page.Page;

/**
 * @ClassName CoreDaoImpl
 * @Description 核心DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
public class CoreDaoImpl implements CoreDao {

	/**
	 * @Fields sqlMapClientTemplate : Spring SqlMapClientTemplate 模板
	 */
	private SqlMapClientTemplate sqlMapClientTemplate;
	private LogDao log;

	@Override
	public void create(Object object) {
//		jdbcTemplate.save(object);
	}

	@Override
	public List<Object> retrieve(Object object) {
		return this.retrieve(Where.beanToWhere(object));
	}

	@Override
	public List<Object> retrieve(Where where) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE
				+ Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		return this.findByHqlPage(hql, null, where.getParams());
	}

	@Override
	public List<Object> retrieve(Where where, Page page) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE
				+ Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		log.info("hql:"+hql);
		log.info("par:"+where.getParamList());
		log.info("page:"+(null!=page?page.toString():""));
		return this.findByHqlPage(hql, page, where.getParams());
	}

	@Override
	public void update(Object object) {
//		jdbcTemplate.update(object);
	}

	@Override
	public void delete(Object object) {
//		jdbcTemplate.delete(object);
	}

	@Override
	public void delete(java.util.Collection<?> collection) {
//		jdbcTemplate.deleteAll(collection);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long count(Where where) {
//		String hql = "select count(*) from "+where.getEntityName()+" where 1=1 "+where.queryString();
		List<Object> list = new ArrayList<Object>();
		if(list != null && !list.isEmpty() && null!=list.get(0)){
			return Long.parseLong(list.get(0).toString());
		}else{
			return 0;
		}
	}

	private List<Object> findByHqlPage(final String hql, final Page page, final Object... parms) {
		return new ArrayList<Object>();
	}

	public SqlMapClientTemplate getSqlMapClientTemplate() {
		return sqlMapClientTemplate;
	}

	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMapClientTemplate = sqlMapClientTemplate;
	}

	public LogDao getLog() {
		return log;
	}

	public void setLog(LogDao log) {
		this.log = log;
	}

}
