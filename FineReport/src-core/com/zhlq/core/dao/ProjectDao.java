package com.zhlq.core.dao;

import com.zhlq.condition.Where;

/**
 * @ClassName ProjectDao
 * @Description 项目Dao
 * @author ZHLQ
 * @date 2014年9月6日 下午12:14:34
 */
public interface ProjectDao extends BaseDao {

	Object retrieveOne(Object object);

	Object retrieveOne(Where where);

	Object retrieveOneException(Object object);

	Object retrieveOneException(Where where);

	Object retrieveOneForce(Object object);

	Object retrieveOneForce(Where where);

	Object retrieveOneForceException(Object object);

	Object retrieveOneForceException(Where where);

	/**
	 * @Title updateNotNullStr
	 * @Description 修改对象
	 * @适用条件：修改对象时
	 * @使用方法：实体类调用
	 * @注意事项：不修改为null的字符串
	 * @param object需要修改对象
	 * @return void 返回类型
	 */
	void updateNotNullStr(Object object);

}
