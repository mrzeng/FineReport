package com.zhlq.core.dao.jdbctmpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.zhlq.condition.Sql;
import com.zhlq.condition.Where;
import com.zhlq.log.dao.LogDao;
import com.zhlq.page.Page;

/**
 * @ClassName CoreDaoImpl
 * @Description 核心DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
public class CoreDaoImpl implements CoreDao {

	/**
	 * @Fields jdbcTemplate : spring jdbc 模板
	 */
	private JdbcTemplate jdbcTemplate;
	private LogDao log;

	@Override
	public void create(Object object) {
//		jdbcTemplate.save(object);
	}

	@Override
	public List<Object> retrieve(Object object) {
		return this.retrieve(Where.beanToWhere(object));
	}

	@Override
	public List<Object> retrieve(Where where) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE
				+ Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		return this.findByHqlPage(hql, null, where.getParams());
	}

	@Override
	public List<Object> retrieve(Where where, Page page) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE
				+ Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		log.info("hql:"+hql);
		log.info("par:"+where.getParamList());
		log.info("page:"+(null!=page?page.toString():""));
		return this.findByHqlPage(hql, page, where.getParams());
	}

	@Override
	public void update(Object object) {
//		jdbcTemplate.update(object);
	}

	@Override
	public void delete(Object object) {
//		jdbcTemplate.delete(object);
	}

	@Override
	public void delete(java.util.Collection<?> collection) {
//		jdbcTemplate.deleteAll(collection);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long count(Where where) {
		String hql = "select count(*) from "+where.getEntityName()+" where 1=1 "+where.queryString();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<Object> list = jdbcTemplate.query(hql, where.getParams(), new RowMapper(){

			@Override
			public Object mapRow(ResultSet arg0, int arg1) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		if(list != null && !list.isEmpty() && null!=list.get(0)){
			return Long.parseLong(list.get(0).toString());
		}else{
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	private List<Object> findByHqlPage(final String hql, final Page page, final Object... parms) {
		@SuppressWarnings({ "rawtypes" })
		List<Object> list = jdbcTemplate.query(hql, parms, new RowMapper(){

			@Override
			public Object mapRow(ResultSet arg0, int arg1) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		return list;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public LogDao getLog() {
		return log;
	}

	public void setLog(LogDao log) {
		this.log = log;
	}

}
