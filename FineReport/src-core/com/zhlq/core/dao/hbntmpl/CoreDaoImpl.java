package com.zhlq.core.dao.hbntmpl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.zhlq.condition.Sql;
import com.zhlq.condition.Where;
import com.zhlq.log.dao.LogDao;
import com.zhlq.page.Page;

/**
 * @ClassName CoreDaoImpl
 * @Description 核心DaoImpl
 * @author ZHLQ
 * @date 2014年9月11日 下午9:01:57
 */
public class CoreDaoImpl implements CoreDao {

	/**
	 * @Fields hibernateTemplate : Hibernate模板
	 */
	private HibernateTemplate hibernateTemplate;
	private LogDao log;

	@Override
	public void create(Object object) {
		hibernateTemplate.save(object);
	}

	public void createOrUpdate(Object object) {
		hibernateTemplate.saveOrUpdate(object);
	}

	@Override
	public List<Object> retrieve(Object object) {
		return this.retrieve(Where.beanToWhere(object));
	}

	@Override
	public List<Object> retrieve(Where where) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE
				+ Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		return this.findByHqlPage(hql, null, where.getParams());
	}

	@Override
	public List<Object> retrieve(Where where, Page page) {
		String hql = Sql.FROM + Sql.SPACE + where.getEntityName() + Sql.SPACE
				+ Sql.WHERE_TRUE + Sql.SPACE + where.queryString();
		log.info("hql:"+hql);
		log.info("para:"+where.getParamList());
		log.info("page:"+(null!=page?page.toString():""));
		return this.findByHqlPage(hql, page, where.getParams());
	}

	@Override
	public void update(Object object) {
		hibernateTemplate.update(object);
	}

	@Override
	public void delete(Object object) {
		hibernateTemplate.delete(object);
	}

	@Override
	public void delete(java.util.Collection<?> collection) {
		hibernateTemplate.deleteAll(collection);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long count(Where where) {
		String hql = "select count(*) from "+where.getEntityName()+" where 1=1 "+where.queryString();
		@SuppressWarnings("unchecked")
		List<Object> list = hibernateTemplate.find(hql, where.getParams());	
		if(list != null && !list.isEmpty() && null!=list.get(0)){
			return Long.parseLong(list.get(0).toString());
		}else{
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	private List<Object> findByHqlPage(final String hql, final Page page, final Object... parms) {
		return hibernateTemplate.executeFind(new HibernateCallback<Object>() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Query query = session.createQuery(hql);
				if(null != page){
					query.setFirstResult((int) (page.getSize() * page.getIndex()));
					query.setMaxResults((int) page.getSize());					
				}
				for (int i = 0; i < parms.length; i++) {
					query.setParameter(i, parms[i]);
				}
				return query.list();
			}

		});
		
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public LogDao getLog() {
		return log;
	}

	public void setLog(LogDao log) {
		this.log = log;
	}

}
