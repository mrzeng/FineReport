package com.zhlq.core.service;

import com.zhlq.condition.Where;
import com.zhlq.exception.DataException;

/**
 * @ClassName ProjectService
 * @Description 项目Service
 * @author ZHLQ
 * @date 2014年8月30日 上午2:03:52
 */
public interface ProjectService extends BaseService {

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要查询的对象
	 * @return Object 返回类型
	 */
	Object retrieveOne(Object object);
	
	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件
	 * @return Object 返回类型
	 */
	Object retrieveOne(Where where);

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要查询的对象
	 * @return Object 返回类型
	 */
	Object retrieveOneException(Object object) throws DataException;
	
	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件
	 * @return Object 返回类型
	 */
	Object retrieveOneException(Where where) throws DataException;

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要查询的对象
	 * @return Object 返回类型
	 */
	Object retrieveOneForce(Object object);

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件
	 * @return Object 返回类型
	 */
	Object retrieveOneForce(Where where);

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param object需要查询的对象
	 * @return Object 返回类型
	 */
	Object retrieveOneForceException(Object object) throws DataException;

	/**
	 * @Title retrieve
	 * @Description 查找对象
	 * @适用条件：查找对象时
	 * @使用方法：实体类调用
	 * @注意事项：参数类型
	 * @param where查询条件
	 * @return Object 返回类型
	 */
	Object retrieveOneForceException(Where where) throws DataException;

	/**
	 * @Title updateNotNullStr
	 * @Description 修改对象
	 * @适用条件：修改对象时
	 * @使用方法：实体类调用
	 * @注意事项：不修改为null的字符串
	 * @param object需要修改对象
	 * @return void 返回类型
	 */
	void updateNotNullStr(Object object);
	
}
