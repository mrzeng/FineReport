package com.zhlq.export.service.excel.jxl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import com.zhlq.export.bean.Cell;
import com.zhlq.export.bean.Data;
import com.zhlq.export.bean.Header;
import com.zhlq.export.bean.Row;
import com.zhlq.export.exception.ExcelException;
import com.zhlq.export.service.excel.ExcelExportService;
import com.zhlq.export.service.impl.ExportServiceImpl;

/**
 * 读取Excel文件工具类
 * 
 */
public class ExcelExportServiceImpl extends ExportServiceImpl implements ExcelExportService {
	
	private WritableSheet writableSheet;
	private WritableWorkbook writableWorkbook;
	private OutputStream outputStream;
	private File file;
	private String path;

	@Override
	public void writeHeader(Header header) {
		this.writeHeader(header, this.getCurrentWritableSheet());
	}

	@Override
	public void writeHeader(Header header, WritableSheet writableSheet) {
		List<Cell> cells = header.getCells();
		if(null==cells || cells.isEmpty()){
			// 空单元格行
			return ;
		}
		Cell cell;
		for(int i = 0; i < cells.size(); i++){
			// 遍历单元格行
			cell = cells.get(i);
			if(null == cell){
				// 空单元格
				continue;
			} else if (null == cell.getString()) {
				// 内容为null
				cell.setString("");
			}
			try {
				// 添加到工作表
				writableSheet.addCell(cell);
			} catch (WriteException e) {
				throw new ExcelException(e);
			}
		}
	}

	@Override
	public void writeHeader(Header header, WritableWorkbook writableWorkbook) {
		this.writeHeader(header, this.getEmptyWritableSheet(writableWorkbook));
	}

	/**
	 * getEmptyWritableSheet
	 *
	 * @Title getEmptyWritableSheet
	 * @Description 获取空工作表
	 * @param @param writableWorkbook
	 * @param @return 设定文件
	 * @return WritableSheet 返回类型
	 * @throws
	 */
	protected WritableSheet getEmptyWritableSheet(WritableWorkbook writableWorkbook) {
		WritableSheet[] sheets = writableWorkbook.getSheets();
		if(null==sheets || 0==sheets.length){
			// 空工作簿
			return writableWorkbook.createSheet("", 0);
		}
		WritableSheet writableSheet = null;
		for(int i = sheets.length - 1; i >= 0; i--){
			// 遍历所有工作表
			writableSheet = sheets[i];
			if(null == writableSheet){
				// 工作表为null
				continue;
			} else if(1 < writableSheet.getRows()) {
				// 工作表行为0
				continue;
			} else if(1==writableSheet.getRows() && 1<writableSheet.getColumns()) {
				// 工作表行为1，列为0
				continue;
			} else {
				// 工作表行不为0，列不为0
				break;
			}
		}
		if(null == writableSheet){
			// 工作表为空
			writableSheet = writableWorkbook.createSheet("", 0);
		}
		return writableSheet;
	}

	@Override
	public void writeData(Data data) {
		int rowNum = this.writableSheet.getRows();
		List<Row> rows = data.getRows();
		if(null==rows || rows.isEmpty()){
			// 没数据
			return ;
		}
		
		Row row;
		List<Cell> cells;
		Cell cell;
		for(int i = 0; i < rows.size(); i++){
			// 遍历每行
			row = rows.get(i);
			if(null == row){
				// 行为空
				continue ;
			}
			cells = row.getCells();
			if(null == cells || cells.isEmpty()){
				// 单元格行为空
				continue ;
			}
			for(int j = 0; j < cells.size(); j++){
				// 遍历单元格行
				cell = cells.get(j);
				if(null == cell){
					// 单元格为空
					continue ;
				}
				try {
					// 写入工作表
					this.writableSheet.addCell(cell.copyByRow(cell.getRow()+rowNum));
				} catch (WriteException e) {
					throw new ExcelException(e);
				}
			}
		}
	}

	@Override
	public void close() {
		if(null != this.writableWorkbook){
			try {
				this.writableWorkbook.write();
				this.writableWorkbook.close();
				this.writableWorkbook = null;
				this.writableSheet = null;
				this.outputStream = null;
				this.file = null;
				this.path = null;
			} catch (WriteException | IOException e) {
				throw new ExcelException(e);
			}
		}
	}

	@Override
	public void close(WritableWorkbook writableWorkbook) {
		try {
			writableWorkbook.write();
			writableWorkbook.close();
			writableWorkbook = null;
			this.writableSheet = null;
			this.writableWorkbook = null;
			this.outputStream = null;
			this.file = null;
			this.path = null;
		} catch (WriteException | IOException e) {
			throw new ExcelException(e);
		}
	}

	@Override
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}
	
	/**
	 * getCurrentWritableSheet
	 *
	 * @Title getCurrentWritableSheet
	 * @Description 获取当前工作表
	 * @param @param writableWorkbook
	 * @param @return 设定文件
	 * @return WritableSheet 返回类型
	 * @throws
	 */
	protected WritableSheet getCurrentWritableSheet(WritableWorkbook writableWorkbook) {
		WritableSheet[] sheets = writableWorkbook.getSheets();
		if(null==sheets || 0==sheets.length){
			// 空工作簿
			return writableWorkbook.createSheet("0", 0);
		}
		WritableSheet writableSheet = null;
		for(int i = sheets.length - 1; i >= 0; i--){
			// 遍历所有工作表
			writableSheet = sheets[i];
			if(null == writableSheet){
				// 工作表为null
				continue;
			} else if(0 <= writableSheet.getRows()) {
				// 工作表行为0
				continue;
			} else {
				// 工作表行大于0
				break;
			}
		}
		if(null == writableSheet){
			// 工作表为空
			writableSheet = writableWorkbook.createSheet("0", 0);
		}
		return writableSheet;
	}

	/**
	 * getCurrentWritableSheet
	 *
	 * @Title getCurrentWritableSheet
	 * @Description 获取当前工作表
	 * @param @return 设定文件
	 * @return WritableSheet 返回类型
	 * @throws
	 */
	public WritableSheet getCurrentWritableSheet() {
		if(null != this.writableSheet){
			return this.writableSheet;
		} else if(null != this.getCurrentWritableWorkbook()) {
			return this.writableSheet = this.getCurrentWritableSheet(this.writableWorkbook);
		} else {
			return this.writableSheet;
		}
	}

	/**
	 * getCurrentWritableWorkbook
	 *
	 * @Title getCurrentWritableWorkbook
	 * @Description 获取当前工作表
	 * @param @return 设定文件
	 * @return WritableWorkbook 返回类型
	 * @throws
	 */
	public WritableWorkbook getCurrentWritableWorkbook() {
		if(null != this.writableWorkbook){
			return this.writableWorkbook;
		} else if (null != this.getCurrentOutputStream()){
			try {
				return writableWorkbook = Workbook.createWorkbook(this.outputStream);
			} catch (IOException e) {
				throw new ExcelException(e);
			}
		} else {
			return this.writableWorkbook;
		}
	}
	
	/**
	 * getCurrentOutputStream
	 *
	 * @Title getCurrentOutputStream
	 * @Description 获取当前输出流
	 * @param @return 设定文件
	 * @return OutputStream 返回类型
	 * @throws
	 */
	public OutputStream getCurrentOutputStream(){
		if(null != this.outputStream){
			return this.outputStream;
		} else if(null != this.getCurrentFile()) {
			try {
				return this.outputStream = new FileOutputStream(this.file);
			} catch (FileNotFoundException e) {
				throw new ExcelException(e);
			}
		} else {
			return this.outputStream;
		}
	}

	/**
	 * getCurrentFile
	 *
	 * @Title getCurrentFile
	 * @Description 获取当前文件
	 * @param @return 设定文件
	 * @return File 返回类型
	 * @throws
	 */
	public File getCurrentFile() {
		if(null != this.file){
			return this.file;
		} else if(null != this.getCurrentPath()) {
			return this.file = new File(this.path);
		} else {
			return this.file;
		}
	}

	/**
	 * getCurrentPath
	 *
	 * @Title getCurrentPath
	 * @Description 获取当前路径
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String getCurrentPath() {
		return this.path;
	}

}
