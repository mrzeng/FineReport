package com.zhlq.export.service;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;


public interface ExportService {

	File fileToDisk(MultipartFile file);
}
