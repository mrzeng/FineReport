package com.zhlq.util;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @ClassName FileUtil
 * @Description 文件工具
 * @author ZHLQ
 * @date 2015年6月10日 上午1:03:05
 */
public class FileUtil {

	/**
	 * write
	 *
	 * @Title write
	 * @Description 写文件
	 * @param json
	 * @param path
	 * @throws IOException 设定文件
	 * @return void 返回类型
	 */
	public static void write(String json, String path) throws IOException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(path);
			fos.write(json.getBytes());
		} finally {
			if (null != fos) {
				fos.close();
				fos = null;
			}
		}
	}

}
