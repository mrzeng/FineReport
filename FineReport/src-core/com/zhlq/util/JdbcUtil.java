package com.zhlq.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * JDBC工具类，使用Properties类实现
 * 
 * @author ZHLQ
 * @version v1.0
 */
public class JdbcUtil {
	/** 配置文件 */
	// private static String configFile = "JdbcConfigDerby.properties";
	private static String configFile = "JdbcConfigMysql.properties";
//	private static String configFileBackup = "/WEB-INF/config/jdbc/JdbcConfigMysql.properties";
	// private static String configFile = "JdbcConfigOracle.properties";
	/** 驱动类名称 */
	private static String driver = "";
	/** 数据库名称 */
	private static String dbName = "";
	/** 数据库连接字符串 */
	private static String url = "";
	/** 用户名称 */
	private static String user = "";
	/** 用户密码 */
	private static String password = "";

	static {
		try {
			load(configFile);
			Class.forName(driver);// 加载驱动
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
//			try {
//				load(configFileBackup);
//				Class.forName(driver);// 加载驱动
//			} catch (IOException | ClassNotFoundException e1) {
//				e1.printStackTrace();
//				throw new RuntimeException("加载数据库配置文件失败", e1);
//			}
		}
	}

	/**
	 * 私有无参构造方法，限制new对象
	 */
	private JdbcUtil() {
	}

	/**
	 * 获取数据库连接
	 * 
	 * @return 数据库连接
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		System.out.println(url+user+password);
		return DriverManager.getConnection(url, user, password);
	}

	/**
	 * 释放数据库资源，采用条件判断释放资源
	 * 
	 * @param resultSet
	 *            结果集ResultSet
	 * @param statement
	 *            SQL命令Statement
	 * @param conn
	 *            数据库连接Connection
	 * @throws SQLException
	 */
	public static void free(ResultSet resultSet, Statement statement, Connection connection) throws SQLException {
		if (null != resultSet) {
			try {
				resultSet.close();
			} finally {
				resultSet = null;
			}
		}
		if (null != statement) {
			try {
				statement.close();
			} finally {
				statement = null;
			}
		}
		if (null != connection) {
			try {
				connection.close();
			} finally {
				connection = null;
			}
		}
	}

	/**
	 * 关闭任意个Connection
	 * 
	 * @param connections
	 *            Connection对象数组
	 * @throws SQLException
	 */
	public static void close(Connection... connections) throws SQLException {
		SQLException exception = null;
		for (int i = 0; i < connections.length; i++) {
			if (null != connections[i]) {
				try {
					connections[i].close();
				} catch (SQLException e) {
					e.setNextException(exception);
					exception = e;
				} finally {
					connections[i] = null;
				}
			}
		}

		if (null != exception) {
			throw exception;
		}
	}

	/**
	 * 关闭任意个Statement
	 * 
	 * @param statements
	 *            Statement对象数组
	 * @throws SQLException
	 */
	public static void close(Statement... statements) throws SQLException {
		SQLException exception = null;
		for (int i = 0; i < statements.length; i++) {
			if (null != statements[i]) {
				try {
					statements[i].close();
				} catch (SQLException e) {
					e.setNextException(exception);
					exception = e;
				} finally {
					statements[i] = null;
				}
			}
		}

		if (null != exception) {
			throw exception;
		}
	}

	/**
	 * 关闭任意个ResultSet
	 * 
	 * @param resultSets
	 *            ResultSet对象数组
	 * @throws SQLException
	 */
	public static void close(ResultSet... resultSets) throws SQLException {
		SQLException exception = null;
		for (int i = 0; i < resultSets.length; i++) {
			if (null != resultSets[i]) {
				try {
					resultSets[i].close();
				} catch (SQLException e) {
					e.setNextException(exception);
					exception = e;
				} finally {
					resultSets[i] = null;
				}
			}
		}

		if (null != exception) {
			throw exception;
		}
	}

	/**
	 * 加载属性
	 * @throws IOException 
	 */
	public static void load(String path) throws IOException {
		Properties properties = new Properties();
		InputStream inputStream = null;
		try {
			if(path.startsWith(File.separator) || path.startsWith("/") || path.startsWith("\\") || path.matches("^[a-zA-Z]:[\\w\\W]*")){
				inputStream = new FileInputStream(path);
			} else {
				inputStream = JdbcUtil.class.getClassLoader().getResourceAsStream(path);			
			}
			if(null != inputStream){
				properties.load(inputStream);
				String[] keys = { "driver", "dbName", "url", "user", "password" };
				driver = properties.getProperty(keys[0]);
				dbName = properties.getProperty(keys[1]);
				url = properties.getProperty(keys[2]);
				user = properties.getProperty(keys[3]);
				password = properties.getProperty(keys[4]);
				url += dbName;
			}
		} finally {
			if (null != inputStream) {
				try {
					inputStream.close();
				} finally {
					inputStream = null;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		try {
			JdbcUtil.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
