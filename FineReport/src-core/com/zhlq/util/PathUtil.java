package com.zhlq.util;

import java.io.File;

public final class PathUtil {

	public static String getJavaProjectPath() {
		File file = new File(PathUtil.class.getResource("/").getFile());
		return file.getParentFile().getParent() + File.separator;
	}

	public static void main(String[] args) {
		System.out.println(PathUtil.getJavaProjectPath());
	}

	public static String getFixedPath() {
		return "E:\\platform\\tomcat\\apache-tomcat-8.0.0-auth\\wtpwebapps\\Auth\\";
	}
}
