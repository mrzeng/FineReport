package com.zhlq.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName ClassUtil
 * @Description 类型工具
 * @author ZHLQ
 * @date 2015年5月26日 上午4:05:22
 */
public final class ClassUtil {
	
	private static List<Class<?>> clazzs;
	
	static {
		// 8种基本类型
		clazzs = new ArrayList<Class<?>>();
		clazzs.add(byte.class);
		clazzs.add(short.class);
		clazzs.add(char.class);
		clazzs.add(int.class);
		clazzs.add(float.class);
		clazzs.add(double.class);
		clazzs.add(long.class);
		clazzs.add(boolean.class);
		clazzs.add(Byte.class);
		clazzs.add(Short.class);
		clazzs.add(Character.class);
		clazzs.add(Integer.class);
		clazzs.add(Float.class);
		clazzs.add(Double.class);
		clazzs.add(Long.class);
		clazzs.add(Boolean.class);
	}
	
	/**
	 * 创建一个新的实例 ClassUtil。
	 * <p>Title: ClassUtil</p>
	 * <p>Description: 私有无参构造方法</p>
	 */
	private ClassUtil() {
		super();
	}

	/**
	 * isString
	 *
	 * @Title isString
	 * @Description 是否字符串类型
	 * @param clazz 对象的Class属性
	 * @return boolean 返回类型
	 */
	public static boolean isString(Class<?> clazz){
		if(null == clazz || Object.class.equals(clazz)){
			return false;
		} else if(String.class.equals(clazz)){
			return true;
		} else {
			return isString(clazz.getSuperclass());			
		}
	}
	
	/**
	 * isString
	 *
	 * @Title isString
	 * @Description 是否字符串类型
	 * @param object 判断对象
	 * @return boolean 返回类型
	 */
	public static boolean isString(Object object){
		if(null == object){
			return false;
		} else {
			return isString(object.getClass());
		}
	}
	
	/**
	 * isBaseClass
	 *
	 * @Title isBaseClass
	 * @Description 是否基本类型
	 * @param clazz 类型
	 * @return boolean 返回类型
	 */
	public static boolean isBaseClass(Class<?> clazz){
		return clazzs.contains(clazz);
	}
	
	/**
	 * isBaseClass
	 *
	 * @Title isBaseClass
	 * @Description 是否基本类型
	 * @param object 判断对象
	 * @return boolean 返回类型
	 */
	public static boolean isBaseClass(Object object){
		if(null == object){
			return false;
		} else {			
			return isBaseClass(object.getClass());				
		}
	}
	
	/**
	 * isBaseOrString
	 *
	 * @Title isBaseOrString
	 * @Description 是否基本类型或者字符串
	 * @param clazz 类型
	 * @return boolean 返回类型
	 */
	public static boolean isBaseOrString(Class<?> clazz){
		if(null==clazz || Object.class.equals(clazz)){
			return false;
		} else if(String.class.equals(clazz) || clazzs.contains(clazz)){
			return true;
		} else {
			return isBaseOrString(clazz.getSuperclass());
		}
	}
	
	/**
	 * isBaseOrString
	 *
	 * @Title isBaseOrString
	 * @Description 是否基本类型或者字符串
	 * @param object 判断对象
	 * @return boolean 返回类型
	 */
	public static boolean isBaseOrString(Object object){
		if(null == object){
			return false;
		} else {
			return isBaseOrString(object.getClass());
		}
	}

	/**
	 * isArrayBaseOrString
	 *
	 * @Title isArrayBaseOrString
	 * @Description 是否基本数据类型数组或者字符串数组
	 * @param value 判断对象
	 * @return boolean 返回类型
	 */
	public static boolean isArrayBaseOrString(Object value) {
		if(null!=value && value.getClass().isArray()){
			// 对象不为空且对象是数组
			Object[] objects = (Object[]) value;
			for(Object o : objects){
				// 遍历数组每个对象
				if(!isBaseOrString(o)){
					return false;
				}
			}
			return true;
		} else {
			// 对象为空或者对象不是数组
			return false;
		}
	}

	/**
	 * isCollectionBaseOrString
	 *
	 * @Title isCollectionBaseOrString
	 * @Description 是否集合对象
	 * @param value 判断对象
	 * @return boolean 返回类型
	 */
	public static boolean isCollectionBaseOrString(Object value) {
		if(null!=value && value instanceof Collection){
			// 对象不为空且对象是集合
			Iterator<?> it = ((Collection<?>)value).iterator();
			while(it.hasNext()){
				// 遍历每个对象
				if(!isBaseOrString(it.next())){
					// 是否基本类型或者字符串
					return false;
				}
			}
			return true;
		} else {
			// 对象为空或者不为集合
			return false;			
		}
	}

	/**
	 * isSameListBaseOrString
	 *
	 * @Title isSameListBaseOrString
	 * @Description 列表对象是否相同类型
	 * @param objects 判断对象
	 * @return boolean 返回类型
	 */
	public static boolean isSameListBaseOrString(List<Object> objects) {
		if(null != objects){
			// 参数不为空
			Class<?> clazz ;
			if(objects.isEmpty()){
				// 列表为空
				return false;
			} else if(null==objects.get(0)){
				// 第一个对象为null
				return false;
			} else {
				// 第一个对象不为null
				clazz = objects.get(0).getClass();
				if(!isBaseOrString(clazz)){
					// 第一个对象不是基本类型或者不是字符串
					return false;
				}
			}
			for(int i = 1; i < objects.size(); i++){
				// 遍历每个对象
				if(null == objects.get(i) && null != clazz){
					// 对象为空，类型不为空
					return false;
				} else if(!objects.get(i).getClass().equals(clazz)){
					// 对象的类型和第一个对象不相同
					return false;
				}
			}
			return true;
		} else {
			// 参数为空
			return false;			
		}
	}
}
