package com.zhlq.util;

import java.util.Collection;

/**
 * @ClassName CollectionUtil
 * @Description 集合工具
 * @author ZHLQ
 * @date 2015年5月26日 上午4:23:37
 */
public final class CollectionUtil {

	/**
	 * isEmpty
	 *
	 * @Title isEmpty
	 * @Description 集合是否为空
	 * @param collection 集合对象
	 * @return boolean 返回类型
	 * @throws
	 */
	public static boolean isEmpty(Collection<?> collection){
		return null!=collection && collection.isEmpty();
	}
	
	/**
	 * isEmptyOrNull
	 *
	 * @Title isEmptyOrNull
	 * @Description 集合是否为空或者null
	 * @param collection
	 * @return boolean 返回类型
	 */
	public static boolean isEmptyOrNull(Collection<?> collection){
		if(null==collection || collection.isEmpty()){
			// 集合为null或者元素数量为0
			return true;
		} else {
			boolean flag = true;
			for(Object c : collection){
				// 遍历每个元素
				if(null != c){
					// 元素不为空
					flag = false;
					break;
				}
			}
			return flag;
		}
	}
	
	/**
	 * removeNull
	 *
	 * @Title removeNull
	 * @Description 移除null值
	 * @param collection 集合对象
	 * @return boolean 返回类型
	 */
	public static boolean removeNull(Collection<?> collection){
		if(null != collection){
			// 集合不为null
			for(Object o : collection){
				// 遍历每个元素
				if(null == o){
					// 元素为空
					collection.remove(o);
				}
			}
		}
		return isEmpty(collection);
	}

	/**
	 * sizeEqual
	 *
	 * @Title sizeEqual
	 * @Description 集合大小是否一致
	 * @param size 集合大小
	 * @param collection 集合对象
	 * @return boolean 返回类型
	 */
	public static boolean sizeEqual(Integer size, Collection<?> collection) {
		Integer i = 0;
		if(null != collection) {
			// 集合不为null
			for(Object o : collection){
				// 遍历每个元素
				if(null == o){
					// 元素为空
					i++;
				}
			}
		}
		return i.equals(size);
	}

	/**
	 * 创建一个新的实例 CollectionUtil。
	 * <p>Title: CollectionUtil</p>
	 * <p>Description: 私有无参构造方法</p>
	 */
	private CollectionUtil() {
		super();
	}

}
