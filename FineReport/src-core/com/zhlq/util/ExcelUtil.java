package com.zhlq.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * 读取Excel文件工具类
 * 
 */
public class ExcelUtil {
	/**
	 * 读取Excel文件的内容
	 * 
	 * @param file
	 *            待读取的文件
	 * @return
	 */
	public static String readExcel(File file) {
		StringBuffer sb = new StringBuffer();

		Workbook wb = null;
		try {
			// 构造Workbook（工作薄）对象
			wb = Workbook.getWorkbook(file);
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (wb == null)
			return null;

		// 获得了Workbook对象之后，就可以通过它得到Sheet（工作表）对象了
		Sheet[] sheet = wb.getSheets();

		if (sheet != null && sheet.length > 0) {
			// 对每个工作表进行循环
			for (int i = 0; i < sheet.length; i++) {
				// 得到当前工作表的行数
				int rowNum = sheet[i].getRows();
				for (int j = 0; j < rowNum; j++) {
					// 得到当前行的所有单元格
					Cell[] cells = sheet[i].getRow(j);
					if (cells != null && cells.length > 0) {
						// 对每个单元格进行循环
						for (int k = 0; k < cells.length; k++) {
							// 读取当前单元格的值
							String cellValue = cells[k].getContents();
							sb.append(cellValue + "\t");
						}
					}
					sb.append("\r\n");
				}
				sb.append("\r\n");
			}
		}
		// 最后关闭资源，释放内存
		wb.close();
		return sb.toString();
	}
	
	/**
	 * 读取excel中指定行的数据
	 * @param excelFile		excel文件
	 * @param sheetIndex	指定某个sheet，从0开始
	 * @param beginRow		从第几行开始,从0开始
	 * @param rowsNum		取几行的数据
	 * @return
	 * @throws Exception
	 */
	public static List<List<String>> readLines(File excelFile,int sheetIndex,int beginRow,int rowsNum)throws Exception{
		//判断参数是否正确、合理。
		if(!excelFile.exists() || sheetIndex<0 || beginRow<0 || rowsNum<0) return null;
		// 构造Workbook（工作薄）对象
		Workbook workbook = Workbook.getWorkbook(excelFile);
		Sheet[] sheets = workbook.getSheets();
		//如果没有sheet，或者找不到指定sheet
		if(!isExcelTrue(workbook,sheets,sheetIndex)) return null;
		Sheet sheet = sheets[sheetIndex];
		int totalRows = sheet.getRows();
		if(totalRows<beginRow) return null;
		List<List<String>> returnList= new ArrayList<List<String>>();
		//计算最后一行的行数，如果末位行数比总行数小，则取末位行数，否则就去中行数
		int endIndex = ((beginRow+rowsNum)<totalRows) ? (beginRow+rowsNum) : totalRows;
		for(int i=beginRow;i<endIndex;i++){
			Cell[] cells = sheet.getRow(i);
			int cellsLength = cells.length;
			List<String> cellsList = new ArrayList<String>();
			for(int j=0;j<cellsLength;j++){
				String cellValue = cells[j].getContents();
				if(cellValue==null)
					cellsList.add("");
				else
					cellsList.add(cellValue.trim());
			}
			returnList.add(cellsList);
		}
		// 最后关闭资源，释放内存
		workbook.close();
		return returnList;
	}
	
	/**
	 *  读取excel中指定行的数据
	 * @param excelFile		excel文件
	 * @param sheetIndex	指定某个sheet，从0开始
	 * @param beginRow		从第几行开始,从0开始
	 * @param rowsNum		取几行的数据
	 * @param titleNum		列头行数
	 * @return
	 * @throws Exception
	 */
	public static List<List<String>> readLines(File excelFile,int sheetIndex,int beginRow,int rowsNum,int titleNum)throws Exception{
		//判断参数是否正确、合理。
		if(!excelFile.exists() || sheetIndex<0 || beginRow<0 || rowsNum<0) return null;
		// 构造Workbook（工作薄）对象
		Workbook workbook = Workbook.getWorkbook(excelFile);
		Sheet[] sheets = workbook.getSheets();
		//如果没有sheet，或者找不到指定sheet
		if(!isExcelTrue(workbook,sheets,sheetIndex)) return null;
		Sheet sheet = sheets[sheetIndex];
		int totalRows = sheet.getRows();
		if(totalRows<beginRow) return null;
		List<List<String>> returnList= new ArrayList<List<String>>();
		//计算最后一行的行数，如果末位行数比总行数小，则取末位行数，否则就去中行数
		int endIndex = ((beginRow+rowsNum)<totalRows) ? (beginRow+rowsNum) : totalRows;
		//获得列头的列数
		int titleCellNum = sheet.getRow(titleNum).length;
		for(int i=beginRow;i<endIndex;i++){
			Cell[] cells = sheet.getRow(i);
			int cellsLength = cells.length;
			List<String> cellsList = new ArrayList<String>();
			for(int j=0;j<cellsLength;j++){
				String cellValue = cells[j].getContents();
				if(cellValue==null)
					cellsList.add("");
				else
					cellsList.add(cellValue.trim());
			}
			//补充后面不足的空列
			if(titleCellNum>cellsLength){
				for(int k = 0 ;k<titleCellNum-cellsLength;k++){
					cellsList.add("");
				}
			}
			returnList.add(cellsList);
		}
		// 最后关闭资源，释放内存
		workbook.close();
		return returnList;
	}
	
	/**
	 * 获得某个单元格的内容
	 * @param excelFile		excel文件
	 * @param sheetIndex	指定某个sheet，从0开始
	 * @param rowNum		单元格所在的行，从0开始
	 * @param columnNum		单元格所在的列，从0开始
	 * @return
	 * @throws Exception
	 */
	public static String getCellContent(File excelFile,int sheetIndex,int rowNum,int columnNum)throws Exception{
		//判断参数是否正确、合理。
		if(!excelFile.exists() || sheetIndex<0 || rowNum<=0 || columnNum<0) return null;
		// 构造Workbook（工作薄）对象
		Workbook workbook = Workbook.getWorkbook(excelFile);
		Sheet[] sheets = workbook.getSheets();
		if(!isExcelTrue(workbook,sheets,sheetIndex))
			return null;
		Sheet sheet = sheets[sheetIndex];
		int totalRows = sheet.getRows();
		if(totalRows<rowNum) 
			return null;
		Cell[] cells = sheet.getRow(rowNum);
		int cellsLength = cells.length;
		if(cellsLength<columnNum)
			return null;
		String cellValue = cells[columnNum].getContents();
		return cellValue;
	}
	/**
	 * 判断Excel创建workbook是否成功，判断选择的sheet是否存在。
	 * @param workbook
	 * @param sheets
	 * @param sheetIndex
	 * @return
	 */
	private static boolean isExcelTrue(Workbook workbook,Sheet[] sheets,int sheetIndex){
		if(workbook == null) 
			return false;
		//如果没有sheet，或者找不到指定sheet
		if(sheets.length<=0 || sheets.length< sheetIndex+1) 
			return false;
		else return true;
	}

	/**
	 * 生成一个Excel文件
	 * 
	 * @param fileName
	 *            要生成的Excel文件名
	 */
	public static void writeExcel(String fileName) {
		WritableWorkbook wwb = null;
		try {
			// 首先要使用Workbook类的工厂方法创建一个可写入的工作薄(Workbook)对象
			wwb = Workbook.createWorkbook(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (wwb != null) {
			// 创建一个可写入的工作表
			// Workbook的createSheet方法有两个参数，第一个是工作表的名称，第二个是工作表在工作薄中的位置
			WritableSheet ws = wwb.createSheet("sheet1", 0);

			// 下面开始添加单元格
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 5; j++) {
					// 这里需要注意的是，在Excel中，第一个参数表示列，第二个表示行
					Label labelC = new Label(j, i, "这是第" + (i + 1) + "行，第"
							+ (j + 1) + "列");
					try {
						// 将生成的单元格添加到工作表中
						ws.addCell(labelC);
					} catch (RowsExceededException e) {
						e.printStackTrace();
					} catch (WriteException e) {
						e.printStackTrace();
					}

				}
			}

			try {
				// 从内存中写入文件中
				wwb.write();
				// 关闭资源，释放内存
				wwb.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			}
		}

	}
	
	/**
	 * 取得excel数据行数
	 * @param excelFile		excel文件
	 * @param sheetIndex	指定某个sheet，从0开始
	 * @param headNum	忽略行数（表头行数）
	 * @return
	 * @throws IOException 
	 * @throws BiffException 
	 */
	public static int getDataNum(File excelFile, int sheetIndex, int headNum, int footNum) throws BiffException, IOException {
		//判断参数是否正确、合理。
		if(!excelFile.exists() || headNum<0 || sheetIndex<0) {
			return 0;
		}
		// 构造Workbook（工作薄）对象
		Workbook workbook = Workbook.getWorkbook(excelFile);
		Sheet[] sheets = workbook.getSheets();
		if(!isExcelTrue(workbook, sheets, sheetIndex)) {
			return 0;
		}
		Sheet sheet = sheets[sheetIndex];
		int totalRows = sheet.getRows();
		int result = totalRows - headNum - footNum;
		if(result < 0) {
			result = 0;
		}
		// 最后关闭资源，释放内存
		workbook.close();
		return result;
	}

	/**
	 * readLine
	 *
	 * @Title readLine
	 * @Description 读取excel中某一行数据
	 * @param excelFile excel文件
	 * @param sheetIndex 工作表索引
	 * @param rowNum 行数
	 * @throws BiffException
	 * @throws IOException
	 * @return List<String> 返回类型
	 */
	public static List<String> readLine(File excelFile, int sheetIndex, int rowNum) throws BiffException, IOException {
		//判断参数是否正确、合理。
		if(!excelFile.exists() || sheetIndex<0 || sheetIndex<0 || rowNum<0) {
			return null;
		}
		// 构造Workbook（工作薄）对象
		Workbook workbook = Workbook.getWorkbook(excelFile);
		Sheet[] sheets = workbook.getSheets();
		//如果没有sheet，或者找不到指定sheet
		if(!isExcelTrue(workbook,sheets,sheetIndex)) {
			return null;
		}
		Sheet sheet = sheets[sheetIndex];
		int totalRows = sheet.getRows();
		if(totalRows < rowNum) {
			return null;
		}
		List<String> returnList = new ArrayList<String>();
		Cell[] cells = sheet.getRow(rowNum);
		for(int i = 0; i < cells.length; i++){
			String cellValue = cells[i].getContents();
			returnList.add(null==cellValue?"":cellValue.trim());
		}
		// 最后关闭资源，释放内存
		workbook.close();
		return returnList;
	}
}
