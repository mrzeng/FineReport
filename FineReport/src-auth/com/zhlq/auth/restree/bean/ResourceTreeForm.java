package com.zhlq.auth.restree.bean;

import com.zhlq.auth.function.bean.Function;
import com.zhlq.auth.resource.bean.Resource;
import com.zhlq.auth.role.bean.Role;

public class ResourceTreeForm extends ResourceTree {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private String idCompare;
	private String parentIdCompare;
	private String levelCompare;
	private String sortCompare;
	private String resIdCompare;
	private String funIdCompare;
	private String roleIdCompare;
	private Resource resource;
	private Function function;
	private Role role;
	
	public String getIdCompare() {
		return idCompare;
	}

	public void setIdCompare(String idCompare) {
		this.idCompare = idCompare;
	}

	public String getParentIdCompare() {
		return parentIdCompare;
	}

	public void setParentIdCompare(String parentIdCompare) {
		this.parentIdCompare = parentIdCompare;
	}

	public String getLevelCompare() {
		return levelCompare;
	}

	public void setLevelCompare(String levelCompare) {
		this.levelCompare = levelCompare;
	}

	public String getSortCompare() {
		return sortCompare;
	}

	public void setSortCompare(String sortCompare) {
		this.sortCompare = sortCompare;
	}

	public String getResIdCompare() {
		return resIdCompare;
	}

	public void setResIdCompare(String resIdCompare) {
		this.resIdCompare = resIdCompare;
	}

	public String getFunIdCompare() {
		return funIdCompare;
	}

	public void setFunIdCompare(String funIdCompare) {
		this.funIdCompare = funIdCompare;
	}

	public String getRoleIdCompare() {
		return roleIdCompare;
	}

	public void setRoleIdCompare(String roleIdCompare) {
		this.roleIdCompare = roleIdCompare;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
