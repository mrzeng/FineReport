package com.zhlq.auth.function.ctrl.annotation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zhlq.auth.function.bean.Function;
import com.zhlq.auth.function.bean.FunctionForm;
import com.zhlq.auth.tree.service.TreeService;
import com.zhlq.condition.Query;
import com.zhlq.constant.CtrlConstant;
import com.zhlq.core.service.ProjectService;
import com.zhlq.page.Page;
import com.zhlq.tips.bean.Tips;
import com.zhlq.tips.constant.TipsConstant;
import com.zhlq.util.CollectionUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;

/**
 * @ClassName FunctionCtrl
 * @Description 功能控制
 * @author ZHLQ
 * @date 2015年4月16日 下午9:50:50
 */
@Controller
public class FunctionCtrl {
	
	private static final String AUTH_FUNCTION_TOEDIT = "auth/function/toedit";
	private static final String AUTH_FUNCTION_EDIT = "auth/function/edit";
	private static final String AUTH_FUNCTION_LOOK = "auth/function/look";
	private static final String AUTH_FUNCTION_DELETE = "auth/function/delete";
	private static final String AUTH_FUNCTION_ADD = "auth/function/add";
	private static final String AUTH_FUNCTION_TOADD = "auth/function/toadd";
	private static final String AUTH_FUNCTION_LIST = "auth/function/list";

	@Autowired
	private ProjectService projectService;
	@Autowired
	private TreeService treeService;
	@Autowired
	@Qualifier("functionValidator")
	private ValidatorService validatorService;

	/**
	 * 列表页面
	 */
	@RequestMapping(AUTH_FUNCTION_LIST)
	public String list(HttpServletRequest request, HttpServletResponse response,
			Page page, FunctionForm functionForm) {
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.PAGE_RESULT), projectService.retrieves(new Query(functionForm), page));
		request.setAttribute(CtrlConstant.get(CtrlConstant.QUERY), functionForm);
		return AUTH_FUNCTION_LIST;
	}

	/**
	 * 跳转新增页面
	 */
	@RequestMapping(AUTH_FUNCTION_TOADD)
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return AUTH_FUNCTION_ADD;
	}

	/**
	 * 新增操作
	 */
	@RequestMapping(AUTH_FUNCTION_ADD)
	public String add(HttpServletRequest request, HttpServletResponse response,
			Function function) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.ADD), function);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存数据库
		projectService.create(function);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.FUNCTION_LIST, CtrlConstant.ADD_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 删除操作
	 */
	@RequestMapping(AUTH_FUNCTION_DELETE)
	public String delete(HttpServletRequest request, HttpServletResponse response,
			Function function) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.DELETE), function);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 删除数据
		projectService.delete(projectService.retrieveOne(function));
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.FUNCTION_LIST, CtrlConstant.DELETE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 查看操作
	 */
	@RequestMapping(AUTH_FUNCTION_LOOK)
	public String look(HttpServletRequest request, HttpServletResponse response,
			Function function) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.LOOK), function);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.LOOK), projectService.retrieveOne(function));
		return AUTH_FUNCTION_LOOK;
	}

	/**
	 * 跳转编辑页面
	 */
	@RequestMapping(AUTH_FUNCTION_TOEDIT)
	public String toEdit(HttpServletRequest request, HttpServletResponse response,
			Function function) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.TOEIDT), function);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.UPDATE), projectService.retrieveOne(function));
		return AUTH_FUNCTION_EDIT;
	}

	/**
	 * 编辑操作
	 */
	@RequestMapping(AUTH_FUNCTION_EDIT)
	public String edit(HttpServletRequest request, HttpServletResponse response,
			Function function) {
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.EDIT), function);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存到数据库
		projectService.updateNotNullStr(function);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.FUNCTION_LIST, CtrlConstant.UPDATE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

}
