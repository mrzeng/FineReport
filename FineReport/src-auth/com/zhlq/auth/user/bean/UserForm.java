package com.zhlq.auth.user.bean;

public class UserForm extends User {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private String idCompare;
	private String nameCompare;
	private String passwordCompare;
	private String stateCompare;
	
	private Integer[] ids;

	public String getIdCompare() {
		return idCompare;
	}

	public void setIdCompare(String idCompare) {
		this.idCompare = idCompare;
	}

	public String getNameCompare() {
		return nameCompare;
	}

	public void setNameCompare(String nameCompare) {
		this.nameCompare = nameCompare;
	}

	public String getPasswordCompare() {
		return passwordCompare;
	}

	public void setPasswordCompare(String passwordCompare) {
		this.passwordCompare = passwordCompare;
	}

	public String getStateCompare() {
		return stateCompare;
	}

	public void setStateCompare(String stateCompare) {
		this.stateCompare = stateCompare;
	}

	public Integer[] getIds() {
		return ids;
	}

	public void setIds(Integer[] ids) {
		this.ids = ids;
	}

}
