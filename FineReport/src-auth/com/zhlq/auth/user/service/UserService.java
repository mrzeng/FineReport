package com.zhlq.auth.user.service;

import java.io.File;
import java.util.List;

import com.zhlq.auth.user.bean.User;
import com.zhlq.auth.user.bean.UserForm;
import com.zhlq.core.service.ProjectService;
import com.zhlq.export.bean.Data;
import com.zhlq.export.bean.Header;

public interface UserService extends ProjectService {

	Header userToHeader(UserForm user);

	Data userToData(UserForm user);

	Data listToData(UserForm user);

	List<User> fileToUser(File file);

}
