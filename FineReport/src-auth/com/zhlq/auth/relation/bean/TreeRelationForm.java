package com.zhlq.auth.relation.bean;

import com.zhlq.auth.function.bean.Function;
import com.zhlq.auth.tree.bean.Tree;

public class TreeRelationForm extends TreeRelation {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private Integer currentTreeId;
	private Tree tree;
	private Function function;

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Integer getCurrentTreeId() {
		return currentTreeId;
	}

	public void setCurrentTreeId(Integer currentTreeId) {
		this.currentTreeId = currentTreeId;
	}

	public Tree getTree() {
		return tree;
	}

	public void setTree(Tree tree) {
		this.tree = tree;
	}

}
