package com.zhlq.auth.resource.bean;


public class ResourceForm extends Resource {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private String idCompare;
	private String enameCompare;
	private String cnameCompare;
	private String typeCompare;
	private String remarkCompare;

	public String getIdCompare() {
		return idCompare;
	}

	public void setIdCompare(String idCompare) {
		this.idCompare = idCompare;
	}

	public String getEnameCompare() {
		return enameCompare;
	}

	public void setEnameCompare(String enameCompare) {
		this.enameCompare = enameCompare;
	}

	public String getCnameCompare() {
		return cnameCompare;
	}

	public void setCnameCompare(String cnameCompare) {
		this.cnameCompare = cnameCompare;
	}

	public String getTypeCompare() {
		return typeCompare;
	}

	public void setTypeCompare(String typeCompare) {
		this.typeCompare = typeCompare;
	}

	public String getRemarkCompare() {
		return remarkCompare;
	}

	public void setRemarkCompare(String remarkCompare) {
		this.remarkCompare = remarkCompare;
	}

}
