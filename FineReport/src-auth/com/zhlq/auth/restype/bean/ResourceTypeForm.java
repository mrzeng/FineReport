package com.zhlq.auth.restype.bean;

public class ResourceTypeForm extends ResourceType {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private String idCompare;
	private String enameCompare;
	private String cnameCompare;
	private String valueCompare;
	private String sortCompare;
	private String remarkCompare;

	public String getIdCompare() {
		return idCompare;
	}

	public void setIdCompare(String idCompare) {
		this.idCompare = idCompare;
	}

	public String getEnameCompare() {
		return enameCompare;
	}

	public void setEnameCompare(String enameCompare) {
		this.enameCompare = enameCompare;
	}

	public String getCnameCompare() {
		return cnameCompare;
	}

	public void setCnameCompare(String cnameCompare) {
		this.cnameCompare = cnameCompare;
	}

	public String getValueCompare() {
		return valueCompare;
	}

	public void setValueCompare(String valueCompare) {
		this.valueCompare = valueCompare;
	}

	public String getRemarkCompare() {
		return remarkCompare;
	}

	public void setRemarkCompare(String remarkCompare) {
		this.remarkCompare = remarkCompare;
	}

	public String getSortCompare() {
		return sortCompare;
	}

	public void setSortCompare(String sortCompare) {
		this.sortCompare = sortCompare;
	}

}
