package com.zhlq.auth.tree.service.impl;

import java.util.List;

import com.zhlq.auth.tree.bean.TemplateTree;
import com.zhlq.auth.tree.bean.TemplateTreeRoleFunc;
import com.zhlq.auth.tree.bean.TreeForm;
import com.zhlq.auth.tree.dao.TreeDao;
import com.zhlq.auth.tree.service.TreeService;
import com.zhlq.core.service.impl.ProjectServiceImpl;

public class TreeServiceImpl extends ProjectServiceImpl implements TreeService {

	private TreeDao treeDao;

	@Override
	public TemplateTree getTemplateTree(String name) {
		return treeDao.getTemplateTree(name);
	}

	@Override
	public TemplateTreeRoleFunc getTemplateTreeRoleFunc(String name, String subtree) {
		return treeDao.getTemplateTreeRoleFunc(name, subtree);
	}

	@Override
	public List<TreeForm> getTreeForm(String name) {
		return treeDao.getTreeForm(name);
	}

	@Override
	public List<TreeForm> getTreeForm(TemplateTree authTree) {
		return treeDao.getTreeForm(authTree);
	}

	@Override
	public List<TreeForm> getTreeFormRoleFunc(TemplateTreeRoleFunc authTree, String name, String subtree) {
		return treeDao.getTreeFormRoleFunc(authTree, name, subtree);
	}

	@Override
	public TemplateTree getCurrentNode(TemplateTree authTree, Integer id) {
		if(null==id){
			return authTree;
		} else if(null!=authTree && null!=authTree.getId() && authTree.getId().equals(id)){
			return authTree;
		} else if(null!=authTree && null!=authTree.getChildren() && !authTree.getChildren().isEmpty()){
			TemplateTree tmp = null;
			for(Object o : authTree.getChildren()){
				if(null != o){
					tmp = this.getCurrentNode((TemplateTree) o, id);
					if(null != tmp){
						return tmp;
					}
				}
			}
			return tmp;
		} else {
			return null;
		}
	}

	@Override
	public TemplateTreeRoleFunc getCurrentNode(TemplateTreeRoleFunc authTree, Integer id) {
		if(null==id){
			return authTree;
		} else if(null!=authTree && null!=authTree.getId() && authTree.getId().equals(id)){
			return authTree;
		} else if(null!=authTree && null!=authTree.getChildren() && !authTree.getChildren().isEmpty()){
			TemplateTreeRoleFunc tmp = null;
			for(Object o : authTree.getChildren()){
				if(null != o){
					tmp = this.getCurrentNode((TemplateTreeRoleFunc) o, id);
					if(null != tmp){
						return tmp;
					}
				}
			}
			return tmp;
		} else {
			return null;
		}
	}

	@Override
	public TemplateTree getCurrentNodeRoleFuncs(TemplateTree authTree, Integer id) {
		if(null!=authTree && authTree instanceof TemplateTreeRoleFunc){
			return this.getCurrentNode((TemplateTreeRoleFunc)authTree, id);
		} else {
			return this.getCurrentNode(authTree, id);
		}
	}

	@Override
	public TemplateTree getCurrentNodeRoleFunc(TemplateTreeRoleFunc authTree, Integer id) {
		if(null==id){
			return authTree;
		} else if(null!=authTree && null!=authTree.getId() && authTree.getId().equals(id)){
			return authTree;
		} else if(null!=authTree && null!=authTree.getSubtree() && !authTree.getSubtree().isEmpty()){
			TemplateTree tmp = null;
			for(Object o : authTree.getSubtree()){
				if(null != o){
					tmp = this.getCurrentNodeRoleFuncs((TemplateTree) o, id);
					if(null != tmp){
						return tmp;
					}
				}
			}
			return tmp;
		} else {
			return null;
		}
	}

	@Override
	public String treeToString(TemplateTree tree, TemplateTree current, String type) {
		StringBuilder sb = new StringBuilder();
		if(null!=tree && null!=tree.getId()){
			sb.append("<ul style=\"padding-left: 20px;margin-top: 0px;\">");
				sb.append("<li>");
					sb.append("<div ");
					sb.append((null!=tree.getId() && null!=current && tree.getId().equals(current.getId()))?"class=\"ui-auth-current\"":"");
					sb.append("><input type=\"checkbox\">");
					if(null!=tree.getResource()){
						sb.append("<a href=\"/Auth/auth/tree/");
						sb.append(type);
						sb.append(".do?id=");
						sb.append(tree.getId());
						sb.append("&level=");
						sb.append(tree.getLevel());
						sb.append("\">");
							sb.append(tree.getResource().getCname());
						sb.append("</a>");
					}
					sb.append("</div>");
						if(null!=tree.getChildren() && !tree.getChildren().isEmpty()){
							for(int i = 0;i < tree.getChildren().size(); i++){
								if(null!=tree.getChildren().get(i)){
									sb.append(this.treeToString((TemplateTree)tree.getChildren().get(i), current, type));
								}
							}
						}
				sb.append("</li>");
			sb.append("</ul>");
		}
		return sb.toString();
	}

//	@Override
//	public List<TreeForm> getTreeList(Integer id, String type) {
//		return treeDao.getTreeList(id, type);
//	}
//
//	@Override
//	public TemplateTree getTree(Integer id, String type) {
//		return treeDao.getTree(id, type);
//	}
//	
//	@Override
//	public TemplateTree getTree(Integer roleId) {
//		return treeDao.getTree(roleId);
//	}

//	@Override
//	public String treeToString(TemplateTree tree, TemplateTree currentNode, String type) {
//		StringBuilder sb = new StringBuilder();
//		if(null!=tree && null!=tree.getId()){
//			sb.append("<ul style=\"padding-left: 20px;margin-top: 0px;\">");
//				sb.append("<li>");
//					sb.append("<div ");
//					sb.append((null!=tree.getId() && null!=currentNode && tree.getId().equals(currentNode.getId()))?"class=\"ui-auth-current\"":"");
//					sb.append("><input type=\"checkbox\">");
//					if(null!=tree.getResource()){
//						sb.append("<a href=\"/Auth/auth/tree/");
//						sb.append(type);
//						sb.append(".do?id=");
//						sb.append(tree.getId());
//						sb.append("toResourceTree".equals(type)?"&resId="+tree.getResId():"");
////						sb.append("function/list.do".equals(type)?"&funId="+tree.getFunId():"");
////						sb.append("toRoleTree".equals(type)?"&roleId="+tree.getRoleId():"");
//						sb.append("&level=");
//						sb.append(tree.getLevel());
//						sb.append("\">");
//							sb.append(tree.getResource().getCname());
//						sb.append("</a>");
//					}
//					sb.append("</div>");
//						if(null!=tree.getChildren() && !tree.getChildren().isEmpty()){
//							for(int i = 0;i < tree.getChildren().size(); i++){
//								if(null!=tree.getChildren().get(i)){
//									sb.append(this.treeToString((TemplateTree)tree.getChildren().get(i), currentNode, type));
//								}
//							}
//						}
//				sb.append("</li>");
//			sb.append("</ul>");
//		}
//		return sb.toString();
//	}

//	@Override
//	public TemplateTree resourceTreeFormToAuthTree(TreeForm resourceTreeForm) {
//		TemplateTree authTree = new TemplateTree();
//		try {
//			PropUtil.copyNotNull(authTree, resourceTreeForm);
//		} catch (IllegalAccessException | IllegalArgumentException
//				| InvocationTargetException | IntrospectionException e) {
//			// 反射异常：复制对象
//			(new ReflectException("复制对象", e)).doing();
//		}
//		return authTree;
//	}

//	@Override
//	public void deleteTree(TemplateTree authTree) {
//		treeDao.deleteTree(authTree);
//	}
//
//	@Override
//	public TemplateTree getSubTree(Tree tree) {
//		return treeDao.getSubTree(tree);
//	}

//	@Override
//	public TemplateTree getCurrentNode(TemplateTree authTree, Integer id) {
//		if(null==id || (null!=authTree && null!=authTree.getId() && authTree.getId().equals(id))){
//			return authTree;
//		} else {
//			if(null!=authTree.getChildren() && !authTree.getChildren().isEmpty()){
//				for(Object o : authTree.getChildren()){
//					if(null != o){
//						return this.getCurrentNode((TemplateTree) o, id);
//					}
//				}
//			}
//			// 数据异常：找不到该记录
//			throw new DataException();
//		}
//	}

//	@Override
//	public TreeForm treeToForm(Tree tree) {
//		Where where = Where.beanToWhere(tree);
//		where.setEntityClass(TreeForm.class);
//		return (TreeForm) treeDao.retrieveOneForce(where);
//	}

	public TreeDao getTreeDao() {
		return treeDao;
	}

	public void setTreeDao(TreeDao treeDao) {
		this.treeDao = treeDao;
	}

}
