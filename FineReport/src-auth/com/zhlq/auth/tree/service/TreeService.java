package com.zhlq.auth.tree.service;

import java.util.List;

import com.zhlq.auth.tree.bean.TemplateTree;
import com.zhlq.auth.tree.bean.TemplateTreeRoleFunc;
import com.zhlq.auth.tree.bean.TreeForm;
import com.zhlq.core.service.ProjectService;

public interface TreeService extends ProjectService {

	TemplateTree getTemplateTree(String name);

	TemplateTreeRoleFunc getTemplateTreeRoleFunc(String name, String subtree);

	List<TreeForm> getTreeForm(String name);
	
	List<TreeForm> getTreeForm(TemplateTree authTree);

	List<TreeForm> getTreeFormRoleFunc(TemplateTreeRoleFunc authTree, String name, String subtree);
	
	TemplateTree getCurrentNode(TemplateTree authTree, Integer id);

	TemplateTreeRoleFunc getCurrentNode(TemplateTreeRoleFunc authTree, Integer id);
	
	TemplateTree getCurrentNodeRoleFuncs(TemplateTree authTree, Integer id);
	
	TemplateTree getCurrentNodeRoleFunc(TemplateTreeRoleFunc authTree, Integer id);

	String treeToString(TemplateTree authTree, TemplateTree currentNode, String type);


	
//	TemplateTree getSubTree(Tree tree);

//	List<TreeForm> getTreeList(Integer id, String type);
//
//	TemplateTree getTree(Integer id, String type);
//
//	TemplateTree getTree(Integer roleId);
//
//	TemplateTree resourceTreeFormToAuthTree(TreeForm resourceTree);
//
//	void deleteTree(TemplateTree authTree);
//
//	TreeForm treeToForm(Tree tree);

}
