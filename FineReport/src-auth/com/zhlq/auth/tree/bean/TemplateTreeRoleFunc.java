package com.zhlq.auth.tree.bean;

import java.util.List;

import com.zhlq.auth.function.bean.Function;

public class TemplateTreeRoleFunc extends TemplateTree {
	private Function function;
	private List<TemplateTreeRoleFunc> subtree;

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public List<TemplateTreeRoleFunc> getSubtree() {
		return subtree;
	}

	public void setSubtree(List<TemplateTreeRoleFunc> subtree) {
		this.subtree = subtree;
	}

}
