package com.zhlq.auth.tree.bean;

import com.zhlq.auth.function.bean.Function;
import com.zhlq.auth.resource.bean.Resource;
import com.zhlq.auth.role.bean.Role;

public class AuthTree extends Node {

	private Integer resId;
	private Integer funId;
	private Integer roleId;

	private Resource resource;
	private Function function;
	private Role role;

	public Integer getResId() {
		return resId;
	}

	public void setResId(Integer resId) {
		this.resId = resId;
	}

	public Integer getFunId() {
		return funId;
	}

	public void setFunId(Integer funId) {
		this.funId = funId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
