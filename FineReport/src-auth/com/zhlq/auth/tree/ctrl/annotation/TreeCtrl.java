package com.zhlq.auth.tree.ctrl.annotation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zhlq.auth.restype.bean.ResourceType;
import com.zhlq.auth.tree.bean.Tree;
import com.zhlq.auth.tree.bean.TreeForm;
import com.zhlq.condition.Query;
import com.zhlq.condition.Where;
import com.zhlq.constant.CtrlConstant;
import com.zhlq.core.service.ProjectService;
import com.zhlq.page.Page;
import com.zhlq.tips.bean.Tips;
import com.zhlq.tips.constant.TipsConstant;
import com.zhlq.util.CollectionUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;

/**
 * @ClassName TreeCtrl
 * @Description 资源树控制
 * @author ZHLQ
 * @date 2015年4月16日 下午11:42:18
 */
@Controller
public class TreeCtrl {
	
	private static final String AUTH_TREE_EDIT = "auth/tree/edit";
	private static final String AUTH_TREE_TOEDIT = "auth/tree/toedit";
	private static final String AUTH_TREE_LOOK = "auth/tree/look";
	private static final String AUTH_TREE_DELETE = "auth/tree/delete";
	private static final String AUTH_TREE_ADD = "auth/tree/add";
	private static final String AUTH_TREE_TOADD = "auth/tree/toadd";
	private static final String AUTH_TREE_LIST = "auth/tree/list";
	
	@Autowired
	private ProjectService projectService;
	@Autowired
	@Qualifier("treeValidator")
	private ValidatorService validatorService;

	/**
	 * 列表页面
	 */
	@RequestMapping(AUTH_TREE_LIST)
	public String list(HttpServletRequest request, HttpServletResponse response,
			Page page, TreeForm treeForm) {
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.PAGE_RESULT), 
				projectService.retrieves(new Query(treeForm), page));
		request.setAttribute(CtrlConstant.get(CtrlConstant.QUERY), treeForm);
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_TREE_LIST;
	}

	/**
	 * 跳转新增
	 */
	@RequestMapping(AUTH_TREE_TOADD)
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_TREE_ADD;
	}

	/**
	 * 新增操作
	 */
	@RequestMapping(AUTH_TREE_ADD)
	public String add(HttpServletRequest request, HttpServletResponse response,
			Tree tree) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.ADD), tree);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存数据库
		projectService.create(tree);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.TREE_LIST, CtrlConstant.ADD_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 删除操作
	 */
	@RequestMapping(AUTH_TREE_DELETE)
	public String delete(HttpServletRequest request, HttpServletResponse response,
			Tree tree) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.DELETE), tree);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 删除数据
		projectService.delete(projectService.retrieveOne(tree));
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.TREE_LIST, CtrlConstant.DELETE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 查看操作
	 */
	@RequestMapping(AUTH_TREE_LOOK)
	public String look(HttpServletRequest request, HttpServletResponse response,
			Tree tree) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.LOOK), tree);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.LOOK), projectService.retrieveOne(tree));
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_TREE_LOOK;
	}

	/**
	 * 跳转编辑
	 */
	@RequestMapping(AUTH_TREE_TOEDIT)
	public String toEdit(HttpServletRequest request, HttpServletResponse response,
			Tree tree) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.TOEIDT), tree);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.UPDATE), projectService.retrieveOne(tree));
		// 资源类型
		Where whereResourceType = new Where(ResourceType.class);
		request.setAttribute("resourceTypes", projectService.retrieve(whereResourceType));
		return AUTH_TREE_EDIT;
	}

	/**
	 * 编辑操作
	 */
	@RequestMapping(AUTH_TREE_EDIT)
	public String edit(HttpServletRequest request, HttpServletResponse response,
			Tree tree) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.EDIT), tree);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存到数据库
		projectService.updateNotNullStr(tree);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.TREE_LIST, CtrlConstant.UPDATE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

}
