package com.zhlq.auth.tree.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.tree.bean.Tree;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName TreeValidator
 * @Description 资源树验证
 * @author ZHLQ
 * @date 2015年4月16日 下午11:59:27
 */
@Component("treeValidator")
public class TreeValidator extends ValidatorServiceImpl implements ValidatorService {

	protected static final String TREE_CLOSE = "tree.close";
	protected static final String TREE_OPEN = "tree.open";
	protected static final String TREE_EDIT = "tree.edit";
	protected static final String TREE_TOEIDT = "tree.toeidt";
	protected static final String TREE_LOOK = "tree.look";
	protected static final String TREE_DELETE = "tree.delete";
	protected static final String TREE_ADD = "tree.add";

	{
		map.put(ADD, TREE_ADD);
		map.put(DELETE, TREE_DELETE);
		map.put(LOOK, TREE_LOOK);
		map.put(TOEIDT, TREE_TOEIDT);
		map.put(EDIT, TREE_EDIT);
		map.put(OPEN, TREE_OPEN);
		map.put(CLOSE, TREE_CLOSE);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
			return validateAdd((Tree) object);
		} else if (this.get(DELETE).equals(type)) {
			// 删除
			return validateDelete((Tree) object);
		} else if (this.get(LOOK).equals(type)) {
			// 查看
			return validateLook((Tree) object);
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
			return validateToEdit((Tree) object);
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
			return validateEdit((Tree) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
	}

	/**
	 * 编辑
	 */
	private List<Validator> validateEdit(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		validateId(tree.getId(), validator);
		return validator;
	}

	/**
	 * 跳转编辑
	 */
	private List<Validator> validateToEdit(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		validateId(tree.getId(), validator);
		return validator;
	}

	/**
	 * 查看
	 */
	private List<Validator> validateLook(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		validateId(tree.getId(), validator);
		return validator;
	}

	/**
	 * 删除
	 */
	private List<Validator> validateDelete(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		validateId(tree.getId(), validator);
		return validator;
	}

	/**
	 * 新增
	 */
	private List<Validator> validateAdd(Tree tree) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(tree, validator);
		validateParentId(tree.getParentId(), validator);
		validateLevel(tree.getLevel(), validator);
		validateSort(tree.getSort(), validator);
		return validator;
	}

	/**
	 * 资源主键
	 */
	protected List<Validator> validateResId(Integer resId, List<Validator> validator) {
		if(null == resId){
			validator.add(new Validator("resId", "资源主键", "不能为空"));
		} else if(resId < 1){
			validator.add(new Validator("resId", "资源主键", "必须大于0"));			
		}
		return validator;
	}

	/**
	 * 排序
	 */
	protected List<Validator> validateSort(Integer sort, List<Validator> validator) {
		if(null == sort){
			// 不需要处理
		} else if(sort < 1){
			validator.add(new Validator("sort", "排序", "必须大于0"));			
		}
		return validator;
	}

	/**
	 * 深度
	 */
	protected List<Validator> validateLevel(Integer level, List<Validator> validator) {
		if(null == level){
			validator.add(new Validator("level", "深度", "不能为空"));
		} else if(level < 1){
			validator.add(new Validator("level", "深度", "必须大于0"));			
		}
		return validator;
	}

	/**
	 * 父主键
	 */
	protected List<Validator> validateParentId(Integer parentId, List<Validator> validator) {
		if(null == parentId){
			validator.add(new Validator("parentId", "父主键", "不能为空"));
		} else if(parentId < 1){
			validator.add(new Validator("parentId", "父主键", "必须大于0"));			
		}
		return validator;
	}

}
