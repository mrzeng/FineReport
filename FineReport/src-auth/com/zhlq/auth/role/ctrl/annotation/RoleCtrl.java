package com.zhlq.auth.role.ctrl.annotation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zhlq.auth.role.bean.Role;
import com.zhlq.auth.role.bean.RoleForm;
import com.zhlq.condition.Query;
import com.zhlq.constant.CtrlConstant;
import com.zhlq.core.service.ProjectService;
import com.zhlq.page.Page;
import com.zhlq.tips.bean.Tips;
import com.zhlq.tips.constant.TipsConstant;
import com.zhlq.util.CollectionUtil;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;

/**
 * @ClassName RoleCtrl
 * @Description 角色控制
 * @author ZHLQ
 * @date 2015年4月17日 下午8:10:13
 */
@Controller
public class RoleCtrl {
	
	private static final String AUTH_ROLE_CLOSE = "auth/role/close";
	private static final String AUTH_ROLE_OPEN = "auth/role/open";
	private static final String AUTH_ROLE_EDIT = "auth/role/edit";
	private static final String AUTH_ROLE_TOEDIT = "auth/role/toedit";
	private static final String AUTH_ROLE_LOOK = "auth/role/look";
	private static final String AUTH_ROLE_DELETE = "auth/role/delete";
	private static final String AUTH_ROLE_ADD = "auth/role/add";
	private static final String AUTH_ROLE_TOADD = "auth/role/toadd";
	private static final String AUTH_ROLE_LIST = "auth/role/list";
	
	@Autowired
	private ProjectService projectService;
	@Autowired
	@Qualifier("roleValidator")
	private ValidatorService validatorService;

	/**
	 * 列表页面
	 */
	@RequestMapping(AUTH_ROLE_LIST)
	public String list(HttpServletRequest request, HttpServletResponse response,
			Page page, RoleForm roleForm) {
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.PAGE_RESULT), projectService.retrieves(new Query(roleForm), page));
		request.setAttribute(CtrlConstant.get(CtrlConstant.QUERY), roleForm);
		return AUTH_ROLE_LIST;
	}

	/**
	 * 跳转新增
	 */
	@RequestMapping(AUTH_ROLE_TOADD)
	public String toAdd(HttpServletRequest request, HttpServletResponse response) {
		return AUTH_ROLE_ADD;
	}

	/**
	 * 新增操作
	 */
	@RequestMapping(AUTH_ROLE_ADD)
	public String add(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.ADD), role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存数据库
		projectService.create(role);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.ROLE_LIST, CtrlConstant.ADD_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 删除操作
	 */
	@RequestMapping(AUTH_ROLE_DELETE)
	public String delete(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.DELETE), role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 删除数据
		projectService.delete(projectService.retrieveOne(role));
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.ROLE_LIST, CtrlConstant.DELETE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 查看操作
	 */
	@RequestMapping(AUTH_ROLE_LOOK)
	public String look(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.LOOK), role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.LOOK), projectService.retrieveOne(role));
		return AUTH_ROLE_LOOK;
	}

	/**
	 * 跳转编辑
	 */
	@RequestMapping(AUTH_ROLE_TOEDIT)
	public String toEdit(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.TOEIDT), role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		request.setAttribute(CtrlConstant.get(CtrlConstant.UPDATE), projectService.retrieveOne(role));
		return AUTH_ROLE_EDIT;
	}

	/**
	 * 编辑操作
	 */
	@RequestMapping(AUTH_ROLE_EDIT)
	public String edit(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate(validatorService.get(ValidatorService.EDIT), role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 保存到数据库
		projectService.updateNotNullStr(role);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), 
				new Tips(Tips.SUCCESS, CtrlConstant.ROLE_LIST, CtrlConstant.UPDATE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 启用操作
	 */
	@RequestMapping(AUTH_ROLE_OPEN)
	public String open(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate("role.open", role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		role = (Role) projectService.retrieveOne(role);
		// 保存到数据库
		role.setState("1");
		projectService.update(role);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), new Tips(Tips.SUCCESS, CtrlConstant.ROLE_LIST, CtrlConstant.OPEN_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

	/**
	 * 禁用操作
	 */
	@RequestMapping(AUTH_ROLE_CLOSE)
	public String close(HttpServletRequest request, HttpServletResponse response,
			Role role) {
		// 参数验证
		List<Validator> validatorResult = validatorService.validate("role.close", role);
		if(!CollectionUtil.removeNull(validatorResult)){
			request.setAttribute(CtrlConstant.get(CtrlConstant.VALIDATOR_RESULT), validatorResult);
			return TipsConstant.TIPS_VALIDATOR;
		}
		// 查询数据
		role = (Role) projectService.retrieveOne(role);
		// 保存到数据库
		role.setState("0");
		projectService.update(role);
		request.setAttribute(CtrlConstant.get(CtrlConstant.TIPS), new Tips(Tips.SUCCESS, CtrlConstant.ROLE_LIST, CtrlConstant.CLOSE_SUCCESS));
		return TipsConstant.TIPS_TIPS;
	}

}
