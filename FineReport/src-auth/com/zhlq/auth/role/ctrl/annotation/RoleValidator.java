package com.zhlq.auth.role.ctrl.annotation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.zhlq.auth.role.bean.Role;
import com.zhlq.validator.bean.Validator;
import com.zhlq.validator.service.ValidatorService;
import com.zhlq.validator.service.impl.ValidatorServiceImpl;

/**
 * @ClassName RoleValidator
 * @Description 角色验证
 * @author ZHLQ
 * @date 2015年4月17日 下午8:28:56
 */
@Component("roleValidator")
public class RoleValidator extends ValidatorServiceImpl implements ValidatorService {

	protected static final String ROLE_CLOSE = "role.close";
	protected static final String ROLE_OPEN = "role.open";
	protected static final String ROLE_EDIT = "role.edit";
	protected static final String ROLE_TOEIDT = "role.toeidt";
	protected static final String ROLE_LOOK = "role.look";
	protected static final String ROLE_DELETE = "role.delete";
	protected static final String ROLE_ADD = "role.add";

	{
		map.put(ADD, ROLE_ADD);
		map.put(DELETE, ROLE_DELETE);
		map.put(LOOK, ROLE_LOOK);
		map.put(TOEIDT, ROLE_TOEIDT);
		map.put(EDIT, ROLE_EDIT);
		map.put(OPEN, ROLE_OPEN);
		map.put(CLOSE, ROLE_CLOSE);
	}

	@Override
	public List<Validator> validate(String type, Object object) {
		if (this.get(ADD).equals(type)) {
			// 新增
			return validateAdd((Role) object);
		} else if (this.get(DELETE).equals(type)) {
			// 删除
			return validateDelete((Role) object);
		} else if (this.get(LOOK).equals(type)) {
			// 查看
			return validateLook((Role) object);
		} else if (this.get(TOEIDT).equals(type)) {
			// 跳转编辑
			return validateToEdit((Role) object);
		} else if (this.get(EDIT).equals(type)) {
			// 编辑
			return validateEdit((Role) object);
		} else if (this.get(OPEN).equals(type)) {
			// 启用
			return validateOpen((Role) object);
		} else if (this.get(CLOSE).equals(type)) {
			// 禁用
			return validateClose((Role) object);
		} else {
			// 其他
			return new ArrayList<Validator>();
		}
	}

	/**
	 * 禁用
	 */
	private List<Validator> validateClose(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateId(role.getId(), validator);
		return validator;
	}

	/**
	 * 启用
	 */
	private List<Validator> validateOpen(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateId(role.getId(), validator);
		return validator;
	}

	/**
	 * 编辑
	 */
	private List<Validator> validateEdit(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateId(role.getId(), validator);
		validateEname(role.getEname(), validator);
		validateCname(role.getCname(), validator);
		validateState(role.getState(), validator);
		return validator;
	}

	/**
	 * 跳转编辑
	 */
	private List<Validator> validateToEdit(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateId(role.getId(), validator);
		return validator;
	}

	/**
	 * 查看
	 */
	private List<Validator> validateLook(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateId(role.getId(), validator);
		return validator;
	}

	/**
	 * 删除
	 */
	private List<Validator> validateDelete(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateId(role.getId(), validator);
		return validator;
	}

	/**
	 * 新增
	 */
	private List<Validator> validateAdd(Role role) {
		List<Validator> validator = new ArrayList<Validator>();
		validateObject(role, validator);
		validateEname(role.getEname(), validator);
		validateCname(role.getCname(), validator);
		validateState(role.getState(), validator);
		return validator;
	}

}
