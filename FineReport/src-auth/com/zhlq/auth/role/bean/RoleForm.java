package com.zhlq.auth.role.bean;

public class RoleForm extends Role {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	private String idCompare;
	private String enameCompare;
	private String cnameCompare;
	private String stateCompare;

	public String getIdCompare() {
		return idCompare;
	}

	public void setIdCompare(String idCompare) {
		this.idCompare = idCompare;
	}

	public String getEnameCompare() {
		return enameCompare;
	}

	public void setEnameCompare(String enameCompare) {
		this.enameCompare = enameCompare;
	}

	public String getCnameCompare() {
		return cnameCompare;
	}

	public void setCnameCompare(String cnameCompare) {
		this.cnameCompare = cnameCompare;
	}

	public String getStateCompare() {
		return stateCompare;
	}

	public void setStateCompare(String stateCompare) {
		this.stateCompare = stateCompare;
	}

}
