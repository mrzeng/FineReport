package com.zhlq.auth.bean;



/**
 * Company entity. @author MyEclipse Persistence Tools
 */

public class Company  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String type;
     private String ename;
     private String cname;


    // Constructors

    /** default constructor */
    public Company() {
    }

	/** minimal constructor */
    public Company(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public Company(String no, String type, String ename, String cname) {
        this.no = no;
        this.type = type;
        this.ename = ename;
        this.cname = cname;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public String getEname() {
        return this.ename;
    }
    
    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getCname() {
        return this.cname;
    }
    
    public void setCname(String cname) {
        this.cname = cname;
    }
   








}