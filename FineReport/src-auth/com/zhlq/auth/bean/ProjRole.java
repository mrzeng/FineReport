package com.zhlq.auth.bean;



/**
 * ProjRole entity. @author MyEclipse Persistence Tools
 */

public class ProjRole  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String projNo;
     private String roleNo;


    // Constructors

    /** default constructor */
    public ProjRole() {
    }

	/** minimal constructor */
    public ProjRole(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public ProjRole(String no, String projNo, String roleNo) {
        this.no = no;
        this.projNo = projNo;
        this.roleNo = roleNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getProjNo() {
        return this.projNo;
    }
    
    public void setProjNo(String projNo) {
        this.projNo = projNo;
    }

    public String getRoleNo() {
        return this.roleNo;
    }
    
    public void setRoleNo(String roleNo) {
        this.roleNo = roleNo;
    }
   








}