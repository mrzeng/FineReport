package com.zhlq.auth.bean;



/**
 * GroupUser entity. @author MyEclipse Persistence Tools
 */

public class GroupUser  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String groupNo;
     private String userNo;


    // Constructors

    /** default constructor */
    public GroupUser() {
    }

	/** minimal constructor */
    public GroupUser(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public GroupUser(String no, String groupNo, String userNo) {
        this.no = no;
        this.groupNo = groupNo;
        this.userNo = userNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getGroupNo() {
        return this.groupNo;
    }
    
    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getUserNo() {
        return this.userNo;
    }
    
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
   








}