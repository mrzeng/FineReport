package com.zhlq.auth.bean;



/**
 * DeptGroup entity. @author MyEclipse Persistence Tools
 */

public class DeptGroup  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String deptNo;
     private String groupNo;


    // Constructors

    /** default constructor */
    public DeptGroup() {
    }

	/** minimal constructor */
    public DeptGroup(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public DeptGroup(String no, String deptNo, String groupNo) {
        this.no = no;
        this.deptNo = deptNo;
        this.groupNo = groupNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getDeptNo() {
        return this.deptNo;
    }
    
    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getGroupNo() {
        return this.groupNo;
    }
    
    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }
   








}