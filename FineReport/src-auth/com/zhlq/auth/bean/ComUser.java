package com.zhlq.auth.bean;



/**
 * ComUser entity. @author MyEclipse Persistence Tools
 */

public class ComUser  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String comNo;
     private String userNo;


    // Constructors

    /** default constructor */
    public ComUser() {
    }

	/** minimal constructor */
    public ComUser(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public ComUser(String no, String comNo, String userNo) {
        this.no = no;
        this.comNo = comNo;
        this.userNo = userNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getComNo() {
        return this.comNo;
    }
    
    public void setComNo(String comNo) {
        this.comNo = comNo;
    }

    public String getUserNo() {
        return this.userNo;
    }
    
    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }
   








}