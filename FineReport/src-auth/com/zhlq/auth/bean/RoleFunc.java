package com.zhlq.auth.bean;



/**
 * RoleFunc entity. @author MyEclipse Persistence Tools
 */

public class RoleFunc  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String roleNo;
     private String funcNo;


    // Constructors

    /** default constructor */
    public RoleFunc() {
    }

	/** minimal constructor */
    public RoleFunc(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public RoleFunc(String no, String roleNo, String funcNo) {
        this.no = no;
        this.roleNo = roleNo;
        this.funcNo = funcNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getRoleNo() {
        return this.roleNo;
    }
    
    public void setRoleNo(String roleNo) {
        this.roleNo = roleNo;
    }

    public String getFuncNo() {
        return this.funcNo;
    }
    
    public void setFuncNo(String funcNo) {
        this.funcNo = funcNo;
    }
   








}