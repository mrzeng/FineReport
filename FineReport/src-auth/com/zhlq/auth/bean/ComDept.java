package com.zhlq.auth.bean;



/**
 * ComDept entity. @author MyEclipse Persistence Tools
 */

public class ComDept  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private String no;
     private String comNo;
     private String deptNo;


    // Constructors

    /** default constructor */
    public ComDept() {
    }

	/** minimal constructor */
    public ComDept(String no) {
        this.no = no;
    }
    
    /** full constructor */
    public ComDept(String no, String comNo, String deptNo) {
        this.no = no;
        this.comNo = comNo;
        this.deptNo = deptNo;
    }

   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return this.no;
    }
    
    public void setNo(String no) {
        this.no = no;
    }

    public String getComNo() {
        return this.comNo;
    }
    
    public void setComNo(String comNo) {
        this.comNo = comNo;
    }

    public String getDeptNo() {
        return this.deptNo;
    }
    
    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }
   








}