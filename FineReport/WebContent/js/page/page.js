function page(current, select, event) {
	// 赋值
	if(event){
		// 输入框有按键被按下
		if(13 == event.keyCode){
			// 按下的键为回车键
			size = $("#ui-page-select").val();
			/*size = $(".ui-page > li > select > option:selected").val();*/
			current = $("#ui-page-current").val();
			/*current = $(".ui-page > li > input").val();*/
		} else {
			// 按下的键不是回车键
			current = $("#ui-page-current").val();
			current = current.replace(/[^0-9]/, "");
			$("#ui-page-current").val(current);
			return ;
		}
	} else if(select){
		// 下拉框的值改变了，使用新的值
		size = select.value;
		current = 1;
	} else {
		// 普通页面跳转
		current = current?current:1;
		size = $("#ui-page-select").val();
		/*size = $(".ui-page > li > select > option:selected").val();*/
	}
	
	// 创建dom节点
	var inputCurrent = document.createElement("input");
	inputCurrent.name = "current";
	inputCurrent.type = "hidden";
	inputCurrent.value = current;
	var inputSize = document.createElement("input");
	inputSize.name = "size";
	inputSize.type = "hidden";
	inputSize.value = size;
	
	// 表单
	var formName = "queryForm";
	var form = document.getElementById(formName);
	if(!form) {
		form = document.forms[0];
	}
	
	// 添加dom节点
	form.appendChild(inputCurrent);
	form.appendChild(inputSize);
	
	// 提交表单
	form.submit();
}

function plus(max){
	var current=document.getElementById('ui-page-current');
	if(current.value < max){
		current.value=parseInt(current.value)+1;
	}
}

function minus(){
	var current=document.getElementById('ui-page-current');
	var value = current.value;
	current.value=parseInt(value)>2?parseInt(value)-1:1;
}

function go(){
	page(document.getElementById('ui-page-current').value);
}