var suffix = ".do";

function jump() {
	if (arguments && arguments.length == 1) {
		// 只有一个参数
		if (arguments[0].indexOf(suffix) > 0) {
			// 带有.do的路径
			window.location.href = arguments[0];
		} else {
			// 没有.do的路径
			window.location.href = arguments[0] + suffix;
		}
	} else if (arguments && arguments.length > 1) {
		// 多个参数
		var param = "";
		for (var i = 1; i < arguments.length; i++) {
			param += "&" + arguments[i];
		}
		if (param.length > 1) {
			param = param.substring(1);
		}
		if (arguments[0].indexOf(suffix) > 0) {
			// 带有.do的路径
			window.location.href = arguments[0] + "?" + param;
		} else {
			// 没有.do的路径
			window.location.href = arguments[0] + suffix + "?" + param;
		}
	}
}