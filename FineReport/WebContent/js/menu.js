$(document).ready(function() {
	// Store variables
	var menu_head = $('.ui-menu>li>a'),
		menu_body = $('.ui-menu>li>ul');
	
	menu_head.each(function(index){
		$(this).data("menuIndex",index+1);
	});

	// Open the first tab on load
	if(typeof(window.sessionStorage.getItem('menuFirst')) == "undefined"){
		sessionStorage.menuFirst = 1;
		menu_head.first().addClass('ui-menu-active').next().find('ui-menu-normal').slideToggle();
	} else {
		menu_head.each(function(){
			if(window.sessionStorage.getItem('menuFirst') == $(this).data("menuIndex")){
				menu_body.slideUp('ui-menu-normal');
				$(this).next().stop(true,true).slideToggle('ui-menu-normal');
				menu_head.removeClass('ui-menu-active');
				$(this).addClass('ui-menu-active');
				sessionStorage.menuFirst = $(this).data("menuIndex");
			} else {
				
			}
		});
	}

	// Click function
	menu_head.on('click', function(event) {
		// Disable header links
		event.preventDefault();

		// Show and hide the tabs on click
		if(window.sessionStorage.getItem('menuFirst') != $(this).data("menuIndex")){
			menu_body.slideUp('ui-menu-normal');
			$(this).next().stop(true,true).slideToggle('ui-menu-normal');
			menu_head.removeClass('ui-menu-active');
			$(this).addClass('ui-menu-active');
			sessionStorage.menuFirst = $(this).data("menuIndex");
		}
	});
});