/**
 * 初始化td里的第一个select
 */
(function(){
	var selects = $(".ui-query>tbody>tr>td>select:nth-child(1)");
	selects.each(function(){
		$(this).hover(function(){
			$(this).attr("title",$(this).find("option:selected").text());
		});
	});
})(jQuery);