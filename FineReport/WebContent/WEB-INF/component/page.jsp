<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/component/page.css" rel="stylesheet" type="text/css" />
<script src="${ctx }js/page/page.js" type="text/javascript"></script>
<!-- 初始化Page对象 -->
<%@ page import="com.zhlq.page.Page" %>
<c:if test="${empty page }">
	<jsp:useBean id="pageTemp" class="com.zhlq.page.Page"></jsp:useBean>
	<jsp:setProperty name="pageTemp" property="current" value="0"/>
	<jsp:setProperty name="pageTemp" property="size" value="0"/>
	<jsp:setProperty name="pageTemp" property="count" value="0"/>
	<jsp:setProperty name="pageTemp" property="index" value="0"/>
	<jsp:setProperty name="pageTemp" property="maxPage" value="0"/>
	<jsp:setProperty name="pageTemp" property="hasLast" value="0"/>
	<jsp:setProperty name="pageTemp" property="hasNext" value="0"/>
	<jsp:setProperty name="pageTemp" property="last" value="0"/>
	<jsp:setProperty name="pageTemp" property="next" value="0"/>
	<jsp:setProperty name="pageTemp" property="maxShow" value="0"/>
	<jsp:setProperty name="pageTemp" property="showBegin" value="0"/>
	<jsp:setProperty name="pageTemp" property="showEnd" value="0"/>
	<jsp:setProperty name="pageTemp" property="showEnd" value="0"/>
	<jsp:setProperty name="pageTemp" property="showLast" value="0"/>
	<jsp:setProperty name="pageTemp" property="showNext" value="0"/>
	<c:set var="page" value="${pageTemp }" />
</c:if>
<!-- 构造分页栏 -->
<div class="ui-page-div">
	<ul class="ui-page">
		<li><a title="首页" href="javascript:page(1);">|&lt;</a></li>
		<li><a title="上一组" href="javascript:page(${page.showLast });">&lt;&lt;</a></li>
		<li><a title="上一页" href="javascript:page(${page.last });">&lt;</a></li>
		<c:forEach begin="${page.showBegin }" end="${page.showEnd }" step="1" var="i">
			<c:set var="liCount" value="${empty liCount ? 1 : liCount+1 }" />
			<li><a ${i eq page.current ? 'style="color:#59B2EE;background-color: #FF0000;"' : '' } href="javascript:page(${i });">${i }</a></li>
		</c:forEach>
		<c:if test="${empty liCount or liCount eq 0 }">
			<li><a href="javascript:page(1);">1</a></li>
		</c:if>
		<li><a title="下一页" href="javascript:page(${page.next });">&gt;</a></li>
		<li><a title="下一组" href="javascript:page(${page.showNext });">&gt;&gt;</a></li>
		<li><a title="末页" href="javascript:page(${page.maxPage });">&gt;|</a></li>
		<li></li>
	</ul>
	<%-- 不要换行 --%>
	<span>共${page.maxPage }页${page.count }条数据&nbsp;每页</span><select id="ui-page-select" onchange="javascript:page(0, this);">
		<%-- 默认每页20条数据 --%>
		<option ${page.size eq 20 ? 'selected' : '' }>20</option>
		<option ${page.size eq 5 ? 'selected' : '' }>5</option>
		<option ${page.size eq 10 ? 'selected' : '' }>10</option>
		<option ${page.size eq 50 ? 'selected' : '' }>50</option>
		<option ${page.size eq 100 ? 'selected' : '' }>100</option>
		<option ${page.size eq 200 ? 'selected' : '' }>200</option>
		<option ${page.size eq 500 ? 'selected' : '' }>500</option>
		<option ${page.size eq 1000 ? 'selected' : '' }>1000</option>
	</select><span>条&nbsp;跳转到第</span><input type="button" value="-" onclick="javascript:minus();" /><input id="ui-page-current" title="输入页数按回车键确认" value="${page.current }" onkeyup="javascript:page(0, 0, event);" onkeydown="javascript:page(0, 0, event);"/><input type="button" value="+" onclick="javascript:plus(${page.maxPage });" /><span>页</span><input type="button" value="go" onclick="javascript:go();" />
</div>