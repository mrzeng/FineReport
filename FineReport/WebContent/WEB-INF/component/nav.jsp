<link href="${ctx }css/component/nav.css" rel="stylesheet" type="text/css" />
<ul class="ui-nav">
	<li><a href="/cas/" data-caption="Cas" ${ctx eq '/cas/' ? 'class="ui-nav-active"' : '' }>Cas</a></li>
	<li><a href="/Base/" data-caption="Base" ${ctx eq '/Base/' ? 'class="ui-nav-active"' : '' }>Base</a></li>
	<li><a href="/Auth/" data-caption="Auth" ${ctx eq '/Auth/' ? 'class="ui-nav-active"' : '' }>Auth</a></li>
	<li><a href="/Chart/" data-caption="Chart" ${ctx eq '/Chart/' ? 'class="ui-nav-active"' : '' }>Chart</a></li>
	<li><a href="/Interface/" data-caption="Interface" ${ctx eq '/Interface/' ? 'class="ui-nav-active"' : '' }>Interface</a></li>
	<li><a href="/FineReport/" data-caption="FineReport" ${ctx eq '/FineReport/' ? 'class="ui-nav-active"' : '' }>FineReport</a></li>
</ul>