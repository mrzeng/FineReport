<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<form id="${not empty jsp.main.list.query.id ? '' : 'queryForm' }" method="post" action="${jsp.main.list.query.action }">
	<table class="ui-query">
		<tbody>
			<tr>
				<td>
					<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
					主键:<select name="idCompare">
						<option value="=" ${query.idCompare eq '='?'selected':'' }>=</option>
						<option value="!=" ${query.idCompare eq '!='?'selected':'' }>!=</option>
						<option value="&lt;" ${query.idCompare eq '>'?'selected':'' }>&lt;</option>
						<option value="&lt;=" ${query.idCompare eq '>='?'selected':'' }>&lt;=</option>
						<option value="&gt;" ${query.idCompare eq '<'?'selected':'' }>&gt;</option>
						<option value="&gt;=" ${query.idCompare eq '<='?'selected':'' }>&gt;=</option>
						<option value="in" ${query.idCompare eq 'in'?'selected':'' }>In</option>
						<option value="not in" ${query.idCompare eq 'not in'?'selected':'' }>NotIn</option>
						<option value="like" ${query.idCompare eq 'like'?'selected':'' }>Like</option>
						<option value="not like" ${query.idCompare eq 'not like'?'selected':'' }>NotLike</option>
						<option value="left like" ${query.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
						<option value="right like" ${query.idCompare eq 'right like'?'selected':'' }>RightLike</option>
						<option value="is null" ${query.idCompare eq 'is null'?'selected':'' }>Null</option>
						<option value="is not null" ${query.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
						<option value="empty" ${query.idCompare eq 'empty'?'selected':'' }>Empty</option>
						<option value="not empty" ${query.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
						<option value="null or empty" ${query.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
						<option value="not (null or empty)" ${query.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
					</select><input name="id" value="${query.id }"/>
				</td>
				<td>
					名称:<select name="nameCompare">
						<option value="=" ${query.nameCompare eq '='?'selected':'' }>=</option>
						<option value="!=" ${query.nameCompare eq '!='?'selected':'' }>!=</option>
						<option value="&lt;" ${query.nameCompare eq '>'?'selected':'' }>&lt;</option>
						<option value="&lt;=" ${query.nameCompare eq '>='?'selected':'' }>&lt;=</option>
						<option value="&gt;" ${query.nameCompare eq '<'?'selected':'' }>&gt;</option>
						<option value="&gt;=" ${query.nameCompare eq '<='?'selected':'' }>&gt;=</option>
						<option value="in" ${query.nameCompare eq 'in'?'selected':'' }>In</option>
						<option value="not in" ${query.nameCompare eq 'not in'?'selected':'' }>NotIn</option>
						<option value="like" ${query.nameCompare eq 'like'?'selected':'' }>Like</option>
						<option value="not like" ${query.nameCompare eq 'not like'?'selected':'' }>NotLike</option>
						<option value="left like" ${query.nameCompare eq 'left like'?'selected':'' }>LeftLike</option>
						<option value="right like" ${query.nameCompare eq 'right like'?'selected':'' }>RightLike</option>
						<option value="is null" ${query.nameCompare eq 'is null'?'selected':'' }>Null</option>
						<option value="is not null" ${query.nameCompare eq 'is not null'?'selected':'' }>NotNull</option>
						<option value="empty" ${query.nameCompare eq 'empty'?'selected':'' }>Empty</option>
						<option value="not empty" ${query.nameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
						<option value="null or empty" ${query.nameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
						<option value="not (null or empty)" ${query.nameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
					</select><input name="name" value="${query.name }"/>
				</td>
				<td>
					密码:<select name="passwordCompare">
						<option value="=" ${query.passwordCompare eq '='?'selected':'' }>=</option>
						<option value="!=" ${query.passwordCompare eq '!='?'selected':'' }>!=</option>
						<option value="&lt;" ${query.passwordCompare eq '>'?'selected':'' }>&lt;</option>
						<option value="&lt;=" ${query.passwordCompare eq '>='?'selected':'' }>&lt;=</option>
						<option value="&gt;" ${query.passwordCompare eq '<'?'selected':'' }>&gt;</option>
						<option value="&gt;=" ${query.passwordCompare eq '<='?'selected':'' }>&gt;=</option>
						<option value="in" ${query.passwordCompare eq 'in'?'selected':'' }>In</option>
						<option value="not in" ${query.passwordCompare eq 'not in'?'selected':'' }>NotIn</option>
						<option value="like" ${query.passwordCompare eq 'like'?'selected':'' }>Like</option>
						<option value="not like" ${query.passwordCompare eq 'not like'?'selected':'' }>NotLike</option>
						<option value="left like" ${query.passwordCompare eq 'left like'?'selected':'' }>LeftLike</option>
						<option value="right like" ${query.passwordCompare eq 'right like'?'selected':'' }>RightLike</option>
						<option value="is null" ${query.passwordCompare eq 'is null'?'selected':'' }>Null</option>
						<option value="is not null" ${query.passwordCompare eq 'is not null'?'selected':'' }>NotNull</option>
						<option value="empty" ${query.passwordCompare eq 'empty'?'selected':'' }>Empty</option>
						<option value="not empty" ${query.passwordCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
						<option value="null or empty" ${query.passwordCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
						<option value="not (null or empty)" ${query.passwordCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
					</select><input name="password" value="${query.password }"/>
				</td>
				<td>
					状态:<select name="stateCompare">
						<option value="=" ${query.stateCompare eq '='?'selected':'' }>=</option>
						<option value="!=" ${query.stateCompare eq '!='?'selected':'' }>!=</option>
						<option value="&lt;" ${query.stateCompare eq '>'?'selected':'' }>&lt;</option>
						<option value="&lt;=" ${query.stateCompare eq '>='?'selected':'' }>&lt;=</option>
						<option value="&gt;" ${query.stateCompare eq '<'?'selected':'' }>&gt;</option>
						<option value="&gt;=" ${query.stateCompare eq '<='?'selected':'' }>&gt;=</option>
						<option value="in" ${query.stateCompare eq 'in'?'selected':'' }>In</option>
						<option value="not in" ${query.stateCompare eq 'not in'?'selected':'' }>NotIn</option>
						<option value="like" ${query.stateCompare eq 'like'?'selected':'' }>Like</option>
						<option value="not like" ${query.stateCompare eq 'not like'?'selected':'' }>NotLike</option>
						<option value="left like" ${query.stateCompare eq 'left like'?'selected':'' }>LeftLike</option>
						<option value="right like" ${query.stateCompare eq 'right like'?'selected':'' }>RightLike</option>
						<option value="is null" ${query.stateCompare eq 'is null'?'selected':'' }>Null</option>
						<option value="is not null" ${query.stateCompare eq 'is not null'?'selected':'' }>NotNull</option>
						<option value="empty" ${query.stateCompare eq 'empty'?'selected':'' }>Empty</option>
						<option value="not empty" ${query.stateCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
						<option value="null or empty" ${query.stateCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
						<option value="not (null or empty)" ${query.stateCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
					</select><select name="state">
						<option value="">请选择</option>
						<option value="1" ${query.state eq '1'?'selected':'' }>是</option>
						<option value="0" ${query.state eq '0'?'selected':'' }>否</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><button>查询</button></td>
			</tr>
		</tbody>
	</table>
</form>