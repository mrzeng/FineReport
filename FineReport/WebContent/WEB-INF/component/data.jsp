<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<table class="ui-operate-top">
	<tbody>
		<tr>
			<td>
				<a class="button small green">全选</a>
				<a class="button small green">反选</a>
				<a class="button small green">批量禁用</a>
				<a class="button small green">批量删除</a>
			</td>
			<td>
				<a href="${ctx }auth/user/toadd.do"><img src="${ctx }img/operate/add.gif">[导出]</a>
				<a href="${ctx }auth/user/toadd.do"><img src="${ctx }img/operate/add.gif">[导入]</a>
				<a href="${ctx }auth/user/toadd.do"><img src="${ctx }img/operate/add.gif">[新增]</a>
			</td>
		</tr>
	</tbody>
</table>
<table class="ui-data">
	<thead>
		<tr>
			<th><input type="checkbox"/></th>
			<th>主键</th>
			<th>名称</th>
			<th>密码</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="item" items="${pageResult.datas }">
			<tr>
				<td><input type="checkbox"/></td>
				<td>${item.id }</td>
				<td>${item.name }</td>
				<td>${item.password }</td>
				<td>
					${item.state eq '1' ? '启用' : '' }
					${item.state eq '0' ? '禁用' : '' }
				</td>
				<td>
					<a href="javascript:jump('${ctx }auth/user/look.do','id=${item.id }');"><img src="${ctx }img/operate/look.gif">[查看]</a>
					<a href="javascript:jump('${ctx }auth/user/toedit.do','id=${item.id }');"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
					<c:if test="${item.state eq '0' }">
						<a href="javascript:jump('${ctx }auth/user/open.do','id=${item.id }');"><span class="ui-operate-open">&nbsp;</span>[启用]</a>									
					</c:if>
					<c:if test="${item.state eq '1' }">
						<a href="javascript:jump('${ctx }auth/user/close.do','id=${item.id }');"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
					</c:if>
					<a href="javascript:jump('${ctx }auth/user/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<table class="ui-operate-middle">
	<tbody>
		<tr><td>
			<a class="button small green">全选</a>
			<a class="button small green">反选</a>
			<a class="button small green">批量禁用</a>
			<a class="button small green">批量删除</a>
		</td></tr>
	</tbody>
</table>