<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th></th>
			<th><img src="${ctx }img/operate/table.gif">角色列表</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form method="post" action="${ctx }auth/role/list.do">
					<table class="ui-query">
						<tbody>
							<tr>
								<td>
									<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
									主键:<select name="idCompare">
										<option value="=" ${query.idCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.idCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.idCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.idCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.idCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.idCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.idCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.idCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.idCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.idCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.idCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.idCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.idCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="id" value="${query.id }"/>
								</td>
								<td>
									英文名称:<select name="enameCompare">
										<option value="=" ${query.enameCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.enameCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.enameCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.enameCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.enameCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.enameCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.enameCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.enameCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.enameCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.enameCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.enameCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.enameCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.enameCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.enameCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.enameCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.enameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.enameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.enameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="ename" value="${query.ename }"/>
								</td>
								<td>
									中文名称:<select name="cnameCompare">
										<option value="=" ${query.cnameCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.cnameCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.cnameCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.cnameCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.cnameCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.cnameCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.cnameCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.cnameCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.cnameCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.cnameCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.cnameCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.cnameCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.cnameCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.cnameCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.cnameCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.cnameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.cnameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.cnameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="cname" value="${query.cname }"/>
								</td>
								<td>
									状态:<select name="stateCompare">
										<option value="=" ${query.stateCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.stateCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.stateCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.stateCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.stateCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.stateCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.stateCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.stateCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.stateCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.stateCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.stateCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.stateCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.stateCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.stateCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.stateCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.stateCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.stateCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.stateCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><select name="state">
										<option value="">请选择</option>
										<option value="1" ${query.state eq '1'?'selected':'' }>是</option>
										<option value="0" ${query.state eq '0'?'selected':'' }>否</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><button>查询</button></td>
							</tr>
						</tbody>
					</table>
				</form>
				<table class="ui-operate-top">
					<tbody>
						<tr>
							<td>
								<a class="button small green">全选</a>
								<a class="button small green">反选</a>
								<a class="button small green">批量禁用</a>
								<a class="button small green">批量删除</a>
							</td>
							<td>
								<a href="${ctx }auth/role/toadd.do"><img src="${ctx }img/operate/add.gif">[导出]</a>
								<a href="${ctx }auth/role/toadd.do"><img src="${ctx }img/operate/add.gif">[导入]</a>
								<a href="${ctx }auth/role/toadd.do"><img src="${ctx }img/operate/add.gif">[新增]</a>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="ui-data">
					<thead>
						<tr>
							<th><input type="checkbox"/></th>
							<th>主键</th>
							<th>英文名称</th>
							<th>中文名称</th>
							<th>状态</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${pageResult.datas }">
							<tr>
								<td><input type="checkbox"/></td>
								<td>${item.id }</td>
								<td>${item.ename }</td>
								<td>${item.cname }</td>
								<td>
									${item.state eq '1' ? '启用' : '' }
									${item.state eq '0' ? '禁用' : '' }
								</td>
								<td>
									<a href="javascript:jump('${ctx }auth/role/look.do','id=${item.id }');"><img src="${ctx }img/operate/look.gif">[查看]</a>
									<a href="javascript:jump('${ctx }auth/role/toedit.do','id=${item.id }');"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
									<c:if test="${item.state eq '0' }">
										<a href="javascript:jump('${ctx }auth/role/open.do','id=${item.id }');"><span class="ui-operate-open">&nbsp;</span>[启用]</a>									
									</c:if>
									<c:if test="${item.state eq '1' }">
										<a href="javascript:jump('${ctx }auth/role/close.do','id=${item.id }');"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
									</c:if>
									<a href="javascript:jump('${ctx }auth/role/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
									<a href="javascript:jump('${ctx }auth/relation/user/toallot.do','relationId=${item.id }');"><img src="${ctx }img/operate/add.gif">[分配资源]</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="ui-operate-middle">
					<tbody>
						<tr><td>
							<a class="button small green">全选</a>
							<a class="button small green">反选</a>
							<a class="button small green">批量禁用</a>
							<a class="button small green">批量删除</a>
						</td></tr>
					</tbody>
				</table>
				<table class="ui-operate-bottom">
					<tbody>
						<tr>
							<td><%@include file="/WEB-INF/component/page.jsp"%></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>