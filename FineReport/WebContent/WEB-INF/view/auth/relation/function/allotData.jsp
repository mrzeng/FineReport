<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/component/tree.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<script src="${ctx }js/auth/tree/add.js" type="text/javascript"></script>
<style>
.table-top>th{
	background-image: -webkit-linear-gradient(to top, #CCFF99, #EAFCD5);
	background-image: linear-gradient(to top, #CCFF99, #EAFCD5);
}
.table-bottom>th{
	border-top: 1px solid #9c6;
	background-image: -webkit-linear-gradient(to top, #EAFCD5, #CCFF99);
	background-image: linear-gradient(to top, #EAFCD5, #CCFF99);
}
<%-- 
.ui-auth-current{
	color: red;
}
.ui-auth-current a{
	color: red;
}
 --%>
</style>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑功能资源树</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td style="text-align: left;vertical-align: top;">
				<table class="ui-list">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th style="width:150px;text-align: left;">
								<img src="${ctx }img/operate/table.gif">功能资源树
							</th>
							<th style="width:100px;text-align: right;">
								<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[批量删除]</a>
							</th>
							<th>&nbsp;</th>
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">功能信息</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="10"></td>
							<td rowspan="10" colspan="2" class="ui-tree">
								<%-- 树形权限-begin --%>
								<c:set var="first" value="${authTreeList[0] }" />
								<c:set var="second" value="${authTreeList[1] }" />
								<%-- 第一个节点-begin --%>
								<c:if test="${not empty first }">
									<ul>
									<li class="ui-tree-double">
										<%-- 是否选中-begin --%>
										<c:set var="checked" value="0" />
										<c:forEach var="tr" items="${treeRelations }">
											<c:if test="${tr.treeId eq first.id }">
												<c:set var="checked" value="1" />
											</c:if>
										</c:forEach>
										<%-- 是否选中-end --%>
										<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
										<a href="">${first.resource.cname }</a>
									</li>
								</c:if>
								<%-- 第一个节点-end --%>
								<%-- 从第二个节点开始，遍历每个节点-begin --%>
								<c:forEach var="currentNode" items="${authTreeList }" begin="2" end="${fn:length(authTreeList)-1 }" step="1">
									<c:if test="${not empty currentNode }">
										<c:if test="${first.level < second.level }">
											<%-- 开始下个子树 --%>
											<ul>
										</c:if>
										<c:if test="${first.level > second.level }">
											<%-- 结束当前子树 --%>
											</ul>
										</c:if>
										<li class="${first.level<second.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level eq currentNode.level?'ui-tree-bottom':'' }${first.level eq second.level and second.level > currentNode.level?'ui-tree-none':'' }${first.level>second.level?'ui-tree-double':'' }">
											<%-- 是否选中-begin --%>
											<c:set var="checked" value="0" />
											<c:forEach var="tr" items="${treeRelations }">
												<c:if test="${tr.treeId eq first.id }">
													<c:set var="checked" value="1" />
												</c:if>
											</c:forEach>
											<%-- 是否选中-end --%>
											<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
											<a href="">${second.resource.cname }</a>
										</li>
										<c:set var="first" value="${second }" />
										<c:set var="second" value="${currentNode }" />
									</c:if>
								</c:forEach>
								<%-- 从第二个节点开始，遍历每个节点-end --%>
								<c:if test="${not empty second }">
									<c:if test="${first.level < second.level }">
										<%-- 开始下个子树 --%>
										<ul>
									</c:if>
									<c:if test="${first.level > second.level }">
										<%-- 结束当前子树 --%>
										</ul>
									</c:if>
									<li class="ui-tree-bottom">
										<%-- 是否选中-begin --%>
										<c:set var="checked" value="0" />
										<c:forEach var="tr" items="${treeRelations }">
											<c:if test="${tr.treeId eq first.id }">
												<c:set var="checked" value="1" />
											</c:if>
										</c:forEach>
										<%-- 是否选中-end --%>
										<input type="checkbox" ${checked eq '1' ? 'checked="checked"' : '' }>
										<a href="">${second.resource.cname }</a>
									</li>
									</ul>
								</c:if>
								<c:if test="${empty second and not empty first }">
									<%-- 结束整颗树 --%>
									</ul>
								</c:if>
								<%-- 树形权限-end --%>
							</td>
							<td rowspan="10"></td>
							<td style="border-left: 1px solid #9c6;"></td>
							<td>
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty function }">
											<tr><td colspan="4">暂无</td></tr>
										</c:if>
										<c:if test="${not empty function }">
											<tr>
												<td>${function.id }</td>
												<td>${function.ename }</td>
												<td>${function.cname }</td>
												<td>${function.remark }</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 当前节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">当前节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>中文名称</th>
											<th>英文名称</th>
											<th>资源类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty currentNode }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:if test="${not empty currentNode }">
											<tr>
												<td>${currentNode.id }</td>
												<td>${currentNode.parentId }</td>
												<td>${currentNode.level }</td>
												<td>${currentNode.resId }</td>
												<td>${currentNode.resource.ename }</td>
												<td>${currentNode.resource.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${currentNode.resource.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[勾选]</a>
												</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 子节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">子节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>中文名称</th>
											<th>英文名称</th>
											<th>资源类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty childNodes or fn:length(childNodes)==0 }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${childNodes }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${item.id }</td>
												<td>${item.parentId }</td>
												<td>${item.level }</td>
												<td>${item.resId }</td>
												<td>${item.resource.ename }</td>
												<td>${item.resource.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.resource.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/relation/function/allot.do','treeId=${item.id }','relationId=${function.id }','currentTreeId=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[勾选]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th></th>
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>