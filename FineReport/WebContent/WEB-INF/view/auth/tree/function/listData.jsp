<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<script src="${ctx }js/auth/tree/add.js" type="text/javascript"></script>
<style>
ul{
	list-style:none;
}
li{
	border-left: 1px solid #9c6;
	border-top: 1px solid #9c6;
}
.ui-list>tbody>tr>td>ul>li:nth-child(1){
	border-right: 1px solid #9c6;
	border-bottom: 1px solid #9c6;
}
.table-top>th{
	background-image: -webkit-linear-gradient(to top, #CCFF99, #EAFCD5);
	background-image: linear-gradient(to top, #CCFF99, #EAFCD5);
}
.table-bottom>th{
	border-top: 1px solid #9c6;
	background-image: -webkit-linear-gradient(to top, #EAFCD5, #CCFF99);
	background-image: linear-gradient(to top, #EAFCD5, #CCFF99);
}
.ui-auth-current{
	color: red;
}
.ui-auth-current a{
	color: red;
}
</style>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑功能资源树</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td style="text-align: left;vertical-align: top;">
				<table class="ui-list">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th style="width:150px;text-align: left;">
								<img src="${ctx }img/operate/table.gif">功能资源树
							</th>
							<th style="width:100px;text-align: right;">
								<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[批量删除]</a>
							</th>
							<th>&nbsp;</th>
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">功能信息</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="10"></td>
							<td rowspan="10" colspan="2" style="text-align: left;vertical-align: top;">
								${treeString }
							</td>
							<td rowspan="10"></td>
							<td style="border-left: 1px solid #9c6;"></td>
							<td>
								<table class="ui-data">
									<thead>
										<tr>
											<th>主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty function }">
											<tr><td colspan="4">暂无</td></tr>
										</c:if>
										<c:if test="${not empty function }">
											<tr>
												<td>${function.id }</td>
												<td>${function.ename }</td>
												<td>${function.cname }</td>
												<td>${function.remark }</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 当前节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">当前节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty currentNode }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:if test="${not empty currentNode }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${currentNode.id }</td>
												<td>${currentNode.parentId }</td>
												<td>${currentNode.level }</td>
												<td>${currentNode.resId }</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/look.do','id=${currentNode.id }');"><img src="${ctx }img/operate/look.gif">[查看]</a>
													<a href="javascript:jump('${ctx }auth/tree/toedit.do','id=${currentNode.id }');"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
													<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${currentNode.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
												</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 子节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">子节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td>
								<form id="addForm" method="post" action="${ctx }auth/tree/function/add.do">
									<table style="width: 100%;">
										<tbody>
											<tr>
												<td>
													英文名称:<input name="resource.ename"/>
												</td>
												<td>
													中文名称:<input name="resource.cname"/>
												</td>
												<td>
													类型:<select name="resource.type" style="width: 155px;">
															<option value="">请选择</option>
															<c:forEach var="resourceType" items="${resourceTypes }">
																<option value="${resourceType.value }">${resourceType.cname }</option>
															</c:forEach>
														</select>
												</td>
												<td>
													<input type="hidden" name="id" value="${currentNode.id }" >
													<input type="button" value="新增" onclick="add()"/>
												</td>
											</tr>
										</tbody>
									</table>
								</form>
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>父主键</th>
											<th>层级</th>
											<th>资源主键</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${empty childNodes or fn:length(childNodes)==0 }">
											<tr><td colspan="8">暂无</td></tr>
										</c:if>
										<c:forEach var="item" items="${childNodes }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${item.id }</td>
												<td>${item.parentId }</td>
												<td>${item.level }</td>
												<td>${item.resId }</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
						<tr class="table-top">
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
						
						<!-- 可选节点 -->
						<tr class="table-bottom">
							<th style="border-left: 1px solid #9c6;">&nbsp;</th>
							<th><img src="${ctx }img/operate/table.gif">可选节点</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td style="border-left: 1px solid #9c6;"></td>
							<td style="text-align: left;vertical-align: top;">
								<form id="queryForm" method="post" action="${ctx }auth/tree/function/query.do">
									<table class="ui-query">
										<tbody>
											<tr>
												<td>
													<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
													主键:<select name="resourceForm.idCompare">
														<option value="=" ${resourceForm.idCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.idCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.idCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.idCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.idCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.idCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.idCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.idCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.idCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.idCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.idCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.idCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.idCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="resourceForm.id" value="${resourceForm.id }"/>
												</td>
												<td>
													英文名称:<select name="resourceForm.enameCompare">
														<option value="=" ${resourceForm.enameCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.enameCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.enameCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.enameCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.enameCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.enameCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.enameCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.enameCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.enameCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.enameCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.enameCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.enameCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.enameCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.enameCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.enameCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.enameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.enameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.enameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="resourceForm.ename" value="${resourceForm.ename }"/>
												</td>
												<td>
													中文名称:<select name="resourceForm.cnameCompare">
														<option value="=" ${resourceForm.cnameCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.cnameCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.cnameCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.cnameCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.cnameCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.cnameCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.cnameCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.cnameCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.cnameCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.cnameCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.cnameCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.cnameCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.cnameCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.cnameCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.cnameCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.cnameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.cnameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.cnameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><input name="resourceForm.cname" value="${resourceForm.cname }"/>
												</td>
												<td>
													类型:<select name="resourceForm.typeCompare">
														<option value="=" ${resourceForm.typeCompare eq '='?'selected':'' }>=</option>
														<option value="!=" ${resourceForm.typeCompare eq '!='?'selected':'' }>!=</option>
														<option value="&lt;" ${resourceForm.typeCompare eq '>'?'selected':'' }>&lt;</option>
														<option value="&lt;=" ${resourceForm.typeCompare eq '>='?'selected':'' }>&lt;=</option>
														<option value="&gt;" ${resourceForm.typeCompare eq '<'?'selected':'' }>&gt;</option>
														<option value="&gt;=" ${resourceForm.typeCompare eq '<='?'selected':'' }>&gt;=</option>
														<option value="in" ${resourceForm.typeCompare eq 'in'?'selected':'' }>In</option>
														<option value="not in" ${resourceForm.typeCompare eq 'not in'?'selected':'' }>NotIn</option>
														<option value="like" ${resourceForm.typeCompare eq 'like'?'selected':'' }>Like</option>
														<option value="not like" ${resourceForm.typeCompare eq 'not like'?'selected':'' }>NotLike</option>
														<option value="left like" ${resourceForm.typeCompare eq 'left like'?'selected':'' }>LeftLike</option>
														<option value="right like" ${resourceForm.typeCompare eq 'right like'?'selected':'' }>RightLike</option>
														<option value="is null" ${resourceForm.typeCompare eq 'is null'?'selected':'' }>Null</option>
														<option value="is not null" ${resourceForm.typeCompare eq 'is not null'?'selected':'' }>NotNull</option>
														<option value="empty" ${resourceForm.typeCompare eq 'empty'?'selected':'' }>Empty</option>
														<option value="not empty" ${resourceForm.typeCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
														<option value="null or empty" ${resourceForm.typeCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
														<option value="not (null or empty)" ${resourceForm.typeCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
													</select><select name="resourceForm.type">
														<option value="">请选择</option>
														<c:forEach var="resourceType" items="${resourceTypes }">
															<option value="${resourceType.value }" ${resourceForm.type eq resourceType.value ? 'selected' : '' }>${resourceType.cname }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>
													<input type="hidden" name="id" value="${currentNode.id }">
													<button>查询</button>
												</td>
											</tr>
										</tbody>
									</table>
								</form>
								<table class="ui-data">
									<thead>
										<tr>
											<th><input type="checkbox"/></th>
											<th>主键</th>
											<th>英文名称</th>
											<th>中文名称</th>
											<th>类型</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${resourcesResult.datas }">
											<tr>
												<td><input type="checkbox"/></td>
												<td>${item.id }</td>
												<td>${item.ename }</td>
												<td>${item.cname }</td>
												<td>
													<c:forEach var="resourceType" items="${resourceTypes }">
														${item.type eq resourceType.value ? resourceType.cname : '' }
													</c:forEach>
												</td>
												<td>
													<a href="javascript:jump('${ctx }auth/tree/function/plus.do','id=${currentNode.id }','resId=${item.id }');"><img src="${ctx }img/operate/add.gif">[添加]</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<table class="ui-operate-bottom">
									<tbody>
										<tr>
											<td>
												<jsp:include page="/WEB-INF/component/page.jsp">
													<jsp:param value="${resourcesResult.page }" name="page"/>
												</jsp:include>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th></th>
							<th style="border-left: 1px solid #9c6;"></th>
							<th>&nbsp;</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>