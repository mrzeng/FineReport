<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">查看角色</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<table class="ui-add">
					<tbody>
						<tr>
							<td>英文名称:</td>
							<td>${look.ename }</td>
						</tr>
						<tr>
							<td>中文名称:</td>
							<td>${look.cname }</td>
						</tr>
						<tr>
							<td>类型:</td>
							<td>
								<c:forEach var="resourceType" items="${resourceTypes }">
									${look.type eq resourceType.value ? resourceType.cname : '' }
								</c:forEach>
							</td>
						</tr>
						<tr>
							<td>备注:</td>
							<td>${look.remark }</td>
						</tr>
						<tr>
							<td colspan="4">
								<input type="button" value="返回" onclick="back()"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>