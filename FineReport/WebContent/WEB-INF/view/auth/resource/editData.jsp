<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/jquery.md5/jQuery.md5.js" type="text/javascript"></script>
<script src="${ctx }js/auth/resource/update.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">编辑角色</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form id="updateForm" method="post" action="${ctx }auth/resource/edit.do">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>英文名称:</td>
								<td><input name="ename" value="${update.ename }"/></td>
							</tr>
							<tr>
								<td>中文名称:</td>
								<td><input name="cname" value="${update.cname }"/></td>
							</tr>
							<tr>
								<td>类型:</td>
								<td>
									<select name="type">
										<option value="">请选择</option>
										<c:forEach var="resourceType" items="${resourceTypes }">
											<option value="${resourceType.value }" ${update.type eq resourceType.value ? 'selected' : '' }>${resourceType.cname }</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<td>备注:</td>
								<td><textarea rows="5" cols="40" name="remark">${update.remark }</textarea></td>
							</tr>
							<tr>
								<td colspan="4">
									<input name="id" value="${update.id }" type="hidden"/>
									<input type="button" value="修改" onclick="update()"/>
									<input type="reset" value="重置"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>