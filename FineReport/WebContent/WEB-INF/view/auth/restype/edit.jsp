<%@ page pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<%-- head begin --%>
<%@include file="/WEB-INF/common/headBegin.jsp" %>
<title>${aTitle }</title>
<%-- head end --%>
<%@include file="/WEB-INF/common/headEnd.jsp" %>
</head>
<body>
	<%-- body begin --%>
	<%@include file="/WEB-INF/common/bodyBegin.jsp" %>	
	<%-- layout --%>
	<table class="ui-layout">
		<tbody>
			<tr>
				<td colspan="2">
					<%-- header --%>
					<%@include file="/WEB-INF/layout/header.jsp" %>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<%-- navigation --%>
					<%@include file="/WEB-INF/component/nav.jsp" %>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<%-- menu --%>
						<%@include file="/WEB-INF/component/menu.jsp" %>
					</div>
					<%-- space --%>
					<div id="ui-layout-space"></div>
				</td>
				<td>
					<%-- data --%>
					<%@include file="/WEB-INF/view/auth/restype/editData.jsp" %>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<%-- footer --%>
					<%@include file="/WEB-INF/layout/footer.jsp" %>
				</td>
			</tr>
		</tbody>
	</table>
	<%-- body end --%>
	<%@include file="/WEB-INF/common/bodyEnd.jsp" %>
</body>
</html>