<%@ page pageEncoding="UTF-8"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/add2.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><img src="${ctx }img/operate/table.gif">查看资源类型</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form action="">
					<table class="ui-add">
						<tbody>
							<tr>
								<td>英文名称:</td>
								<td>${look.ename }</td>
							</tr>
							<tr>
								<td>中文名称:</td>
								<td>${look.cname }</td>
							</tr>
							<tr>
								<td>编号:</td>
								<td>${look.value }</td>
							</tr>
							<tr>
								<td>排序:</td>
								<td>${look.sort }</td>
							</tr>
							<tr>
								<td>备注:</td>
								<td>
									${look.remark }
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="button" value="返回" onclick="back()"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>

<script>
	$(document).ready(function() { $(".ui-add").colorize(); });
</script>