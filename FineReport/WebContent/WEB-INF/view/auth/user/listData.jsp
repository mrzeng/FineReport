<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${ctx }css/list.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/query.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/operate.css" rel="stylesheet" type="text/css"/>
<link href="${ctx }css/data.css" rel="stylesheet" type="text/css"/>
<script src="${ctx }js/link/link.js" type="text/javascript"></script>
<script src="${ctx }js/list/list.js" type="text/javascript"></script>
<table class="ui-list">
	<thead>
		<tr>
			<th></th>
			<th>
				<img src="${ctx }img/operate/table.gif">用户列表
				<form class="list-form"><%-- 列表表单，用于各种操作 --%></form>
			</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td>
				<form id="${not empty applicationScope.mvc.queryForm ? '' : 'queryForm' }" method="post" action="${ctx }auth/user/list.do">
					<table class="ui-query">
						<tbody>
							<tr>
								<td>
									<%-- 属性名/比较符/值，三者之间不要有空白字符 --%>
									主键:<select name="idCompare">
										<option value="=" ${query.idCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.idCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.idCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.idCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.idCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.idCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.idCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.idCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.idCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.idCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.idCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.idCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.idCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.idCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.idCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.idCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.idCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.idCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="id" value="${query.id }"/>
								</td>
								<td>
									名称:<select name="nameCompare">
										<option value="=" ${query.nameCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.nameCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.nameCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.nameCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.nameCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.nameCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.nameCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.nameCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.nameCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.nameCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.nameCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.nameCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.nameCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.nameCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.nameCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.nameCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.nameCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.nameCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="name" value="${query.name }"/>
								</td>
								<td>
									密码:<select name="passwordCompare">
										<option value="=" ${query.passwordCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.passwordCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.passwordCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.passwordCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.passwordCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.passwordCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.passwordCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.passwordCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.passwordCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.passwordCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.passwordCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.passwordCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.passwordCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.passwordCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.passwordCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.passwordCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.passwordCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.passwordCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><input name="password" value="${query.password }"/>
								</td>
								<td>
									状态:<select name="stateCompare">
										<option value="=" ${query.stateCompare eq '='?'selected':'' }>=</option>
										<option value="!=" ${query.stateCompare eq '!='?'selected':'' }>!=</option>
										<option value="&lt;" ${query.stateCompare eq '>'?'selected':'' }>&lt;</option>
										<option value="&lt;=" ${query.stateCompare eq '>='?'selected':'' }>&lt;=</option>
										<option value="&gt;" ${query.stateCompare eq '<'?'selected':'' }>&gt;</option>
										<option value="&gt;=" ${query.stateCompare eq '<='?'selected':'' }>&gt;=</option>
										<option value="in" ${query.stateCompare eq 'in'?'selected':'' }>In</option>
										<option value="not in" ${query.stateCompare eq 'not in'?'selected':'' }>NotIn</option>
										<option value="like" ${query.stateCompare eq 'like'?'selected':'' }>Like</option>
										<option value="not like" ${query.stateCompare eq 'not like'?'selected':'' }>NotLike</option>
										<option value="left like" ${query.stateCompare eq 'left like'?'selected':'' }>LeftLike</option>
										<option value="right like" ${query.stateCompare eq 'right like'?'selected':'' }>RightLike</option>
										<option value="is null" ${query.stateCompare eq 'is null'?'selected':'' }>Null</option>
										<option value="is not null" ${query.stateCompare eq 'is not null'?'selected':'' }>NotNull</option>
										<option value="empty" ${query.stateCompare eq 'empty'?'selected':'' }>Empty</option>
										<option value="not empty" ${query.stateCompare eq 'not empty'?'selected':'' }>NotEmpty</option>
										<option value="null or empty" ${query.stateCompare eq 'null or empty'?'selected':'' }>NullOrEmpty</option>
										<option value="not (null or empty)" ${query.stateCompare eq 'not (null or empty)'?'selected':'' }>Not(NullOrEmpty)</option>
									</select><select name="state">
										<option value="">请选择</option>
										<option value="1" ${query.state eq '1'?'selected':'' }>是</option>
										<option value="0" ${query.state eq '0'?'selected':'' }>否</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
				<script src="${ctx }js/query.js" type="text/javascript"></script>
				<table class="ui-operate-top">
					<tbody>
						<tr>
							<td>
								<a href="javascript:select();" class="button small green">全选</a>
								<a href="javascript:inverse();" class="button small green">反选</a>
								<a href="javascript:look({url:'${website }${ctx }auth/user/look.do',target:'_blank'});" class="button small green">批量查看</a>
								<a href="javascript:edit({url:'${website }${ctx }auth/user/toedit.do',target:'_blank'});" class="button small green">批量编辑</a>
								<a href="javascript:open({url:'${website }${ctx }auth/user/open.do',target:'_blank'});" class="button small green">批量启用</a>
								<a href="javascript:close({url:'${website }${ctx }auth/user/close.do',target:'_blank'});" class="button small green">批量禁用</a>
								<a href="javascript:clopen({url:'${website }${ctx }auth/user/clopen.do',target:'_blank'});" class="button small green">批量反用</a>
								<a href="javascript:deletes({url:'${website }${ctx }auth/user/delete.do',target:'_blank'});" class="button small green">批量删除</a>
								<a href="javascript:exportChoose({url:'${website }${ctx }auth/user/export/choose.do',target:'_self'});" class="button small green">选择导出</a>
								<a href="javascript:exportOption({url:'${website }${ctx }auth/user/export/option.do',target:'_self',queryForm:'#queryForm'});" class="button small green">条件导出</a>
								<a href="javascript:importAdd({url:'${website }${ctx }auth/user/import/add.do',target:'_self'});" class="button small green">新增导入</a>
								<a href="javascript:importEdit({url:'${website }${ctx }auth/user/import/edit.do',target:'_self'});" class="button small green">编辑导入</a>
								<a href="javascript:importAddOrEdit({url:'${website }${ctx }auth/user/import/addedit.do',target:'_self'});" class="button small green">新增或编辑</a>
							</td>
							<td>
								<a href="${ctx }auth/user/toadd.do"><img src="${ctx }img/operate/add.gif">[新增]</a>
								<input type="button" value="高级查询">
								<button form="${not empty applicationScope.mvc.queryForm ? '' : 'queryForm' }">查询</button>
								<input type="button" value="↓" title="展开所有查询条件">
								<input type="button" value="↑" style="display: none;">
								<input type="button" value="字段" title="字段的选择和排序">
							</td>
						</tr>
					</tbody>
				</table>
				<table class="ui-data">
					<thead>
						<tr>
							<th></th>
							<th>主键</th>
							<th>名称</th>
							<th>密码</th>
							<th>状态</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${empty pageResult.datas }">
							<tr><td colspan="6">暂无</td></tr>
						</c:if>
						<c:forEach var="item" items="${pageResult.datas }">
							<tr>
								<td><input type="checkbox" name="checkbox" data-json="{param:['id'],id:'${item.id}'}"/></td>
								<td>${item.id }</td>
								<td>${item.name }</td>
								<td>${item.password }</td>
								<td>
									${item.state eq '1' ? '启用' : '' }
									${item.state eq '0' ? '禁用' : '' }
								</td>
								<td>
									<a href="javascript:jump('${ctx }auth/user/look.do','id=${item.id }');"><img src="${ctx }img/operate/look.gif">[查看]</a>
									<a href="javascript:jump('${ctx }auth/user/toedit.do','id=${item.id }');"><img src="${ctx }img/operate/edit.gif">[编辑]</a>
									<c:if test="${item.state eq '0' }">
										<a href="javascript:jump('${ctx }auth/user/open.do','id=${item.id }');"><span class="ui-operate-open">&nbsp;</span>[启用]</a>									
									</c:if>
									<c:if test="${item.state eq '1' }">
										<a href="javascript:jump('${ctx }auth/user/close.do','id=${item.id }');"><span class="ui-operate-close">&nbsp;</span>[禁用]</a>
									</c:if>
									<a href="javascript:jump('${ctx }auth/user/delete.do','id=${item.id }');"><img src="${ctx }img/operate/delete.gif">[删除]</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="ui-operate-middle">
					<tbody>
						<tr><td>
							<a href="javascript:select();" class="button small green">全选</a>
							<a href="javascript:inverse();" class="button small green">反选</a>
						</td></tr>
					</tbody>
				</table>
				<table class="ui-operate-bottom">
					<tbody>
						<tr>
							<td><%@include file="/WEB-INF/component/page.jsp"%></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th>&nbsp;</th>
			<th></th>
		</tr>
	</tfoot>
</table>